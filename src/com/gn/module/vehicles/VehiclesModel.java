
package com.gn.module.vehicles;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class VehiclesModel extends RecursiveTreeObject<VehiclesModel> {

   
      public StringProperty vehicle_id, vehicle_regNo, vehicle_brand, vehicle_model, vehicle_color, vehicle_type , driver_id ;
  
      
      public VehiclesModel(String vehicle_id, String vehicle_regNo, String vehicle_brand, String vehicle_model,String vehicle_color ,String vehicle_type, String driver_id){
          this.vehicle_id = new SimpleStringProperty(vehicle_id);
          this.vehicle_regNo = new SimpleStringProperty(vehicle_regNo);
          this.vehicle_brand = new SimpleStringProperty(vehicle_brand);
          this.vehicle_model = new SimpleStringProperty(vehicle_model);
          this.vehicle_color =new SimpleStringProperty(vehicle_color);
          this.vehicle_type = new SimpleStringProperty(vehicle_type);
          this.driver_id = new SimpleStringProperty(driver_id);
      }

   public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = new SimpleStringProperty(vehicle_id);
    }
    public void setVehicle_regNo(String vehicle_regNo) {
        this.vehicle_regNo = new SimpleStringProperty(vehicle_regNo);
    }

    public void setVehicle_brand(String vehicle_brand) {
        this.vehicle_brand = new SimpleStringProperty (vehicle_brand);
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = new SimpleStringProperty (vehicle_model);
    }
    
    public void setVehicle_color(String vehicle_color) {
        this.vehicle_color = new SimpleStringProperty (vehicle_color);
    }
    
    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = new SimpleStringProperty (vehicle_type);
    }


    public void setDriver_id(String driver_id) {
        this.driver_id = new SimpleStringProperty (driver_id);
    }

    public String getVehicle_id() {
        return vehicle_id.get();
    }

    public String getVehicle_regNo() {
        return vehicle_regNo.get();
    }

    public String getVehicle_brand() {
        return vehicle_brand.get();
    }
  public String getVehicle_model() {
        return vehicle_model.get();
    }
    public String getVehicle_color() {
        return vehicle_color.get();
    }

    public String getVehicle_type() {
        return vehicle_type.get();
    }


    public String getDriver_id() {
        return driver_id.get();
    }
     
    
}
