package com.gn.module.vehicles;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class EditVehicleController implements Initializable{

    @FXML
    private Label errorLB, regnoLB, DriverLB, brandLB, modelLB, colorLB, typeLB;

    @FXML
    private HBox regnoBOX, colorBOX, typeBox, driverBOX, brandBOX;

    @FXML
    private JFXTextField regnoTF;

   

    @FXML
    private JFXTextField brandTF;

    @FXML
    private HBox modelBOX;

    @FXML
    private JFXTextField modelTF, colorTF;

    @FXML
    private JFXComboBox<String> typeCB, driverCB;
    
    ObservableList<String> DriversData= FXCollections.observableArrayList();
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static  String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs= null;
    
    @FXML
      private void Close(ActionEvent event){
       Button btn = (Button) event.getSource();
      Stage stage =(Stage) btn.getScene().getWindow();
      stage.close();
       
    } 
    private void addEffect(Node node){
        
        
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    

    }
      private void setupListeners(){
        regnoTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRegNo()){
                if(!newValue){
                    Flash swing = new Flash(regnoBOX);
                    regnoLB.setVisible(true);
                    new SlideInLeft(regnoLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    regnoBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    regnoLB.setVisible(false);
                }
            } else {
              errorLB.setVisible(false);
            }
        });
        brandTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validBrandName()){
                if(!newValue){
                    Flash swing = new Flash(brandBOX);
                    brandLB.setVisible(true);
                    new SlideInLeft(brandLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    brandBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    brandLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(false);
            }
        });
        modelTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validModelName()){
                if(!newValue){
                    Flash swing = new Flash(modelBOX);
                    modelLB.setVisible(true);
                    new SlideInLeft(modelLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    modelBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    modelLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(true);
            }
        });
  
        colorTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validColor()){
                if(!newValue){
                    Flash swing = new Flash(colorBOX);
                    colorLB.setVisible(true);
                    new SlideInLeft(colorLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    colorBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    colorLB.setVisible(false);
                }
            }  else {
               errorLB.setVisible(true);
            }
        });

        typeCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validType()){
                if(!newValue){
                    Flash swing = new Flash(typeBox);
                    typeLB.setVisible(true);
                    new SlideInLeft(typeLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    typeBox.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    typeLB.setVisible(false);
                }
            }  else {
            errorLB.setVisible(true);
            }
        });
        driverCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validDriver()){
                if(!newValue){
                    Flash swing = new Flash(driverBOX);
                    DriverLB.setVisible(true);
                    new SlideInLeft(DriverLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    driverBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    DriverLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(true);
            }
        });
        
        }
    //validation for the text fields 
    private boolean validRegNo(){
        return Mask.noSymbols(regnoTF) && !regnoTF.getText().isEmpty() && regnoTF.getLength() > 3  ;

    }
    private boolean validBrandName(){
        return Mask.nameField(brandTF) && Mask.noSymbols(brandTF) && !brandTF.getText().isEmpty() && brandTF.getLength() > 3 ;
    }

    private boolean validModelName(){
        return  Mask.nameField(modelTF) && Mask.noSymbols(modelTF) && !modelTF.getText().isEmpty() && modelTF.getLength() > 3 ;
    }
    private boolean validColor(){
        return Mask.nameField(colorTF) && Mask.noSymbols(colorTF) && !colorTF.getText().isEmpty() && colorTF.getLength() > 3  ;

    }
    private boolean validType(){
        return !typeCB.getSelectionModel().isEmpty();
    }

    private boolean validDriver(){
        return  !driverCB.getSelectionModel().isEmpty();
    }
        //load the driver name on the combo box
              public void getDriverName(){
                 try {
                     String sql="SELECT * from milk_stock.milk_staff where staff_role = 'driver' ";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         DriversData.add(rs.getString("staff_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
              
     String id;         
    public void transferData(String vehicle_id, String  vehicle_regNo, String vehicle_brand, String vehicle_model, String vehicle_color, String vehicle_type , String driver_id ){
       id= vehicle_id;
       regnoTF.setText(vehicle_regNo);
       brandTF.setText(vehicle_brand);
       modelTF.setText(vehicle_model);
       colorTF.setText(vehicle_color);
       typeCB.setValue(vehicle_type);
       driverCB.setValue(driver_id);
    }          
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addEffect(regnoTF);
        addEffect(brandTF);
        addEffect(modelTF);
        addEffect(colorTF);
        addEffect(typeCB);
        addEffect(driverCB);
        
        setupListeners();  
         
        getDriverName();
        
        typeCB.getItems().addAll("PickUp", "Lorry");
        driverCB.setItems(DriversData);
        
         Mask.nameField(brandTF);
         Mask.nameField(modelTF);
         Mask.nameField(colorTF);
         
       
        
    }

@FXML
   private void UpdateVehicle(ActionEvent event){
        if(validRegNo() && validBrandName() && validModelName()&& validColor() && validType() && validDriver()){
            //insert(regnoTF.getText(), brandTF.getText(), modelTF.getText(), colorTF.getText(), typeCB.getValue(), driverCB.getValue() );
            Alerts.info("all CORRECT", "ertyui");
        Button btn = (Button) event.getSource();
      Stage stage = (Stage) btn.getScene().getWindow();
      stage.close();
            
            
        }else if (!validRegNo()){
            regnoLB.setVisible(true);
        } else if (!validBrandName()) {
           brandLB.setVisible(true);
        } else if(!validModelName()){
            modelLB.setVisible(true);
        } else if(!validColor()){
            colorLB.setVisible(true);
        }else if(!validType()){
            typeLB.setVisible(true);
        }else if(!validDriver()){
            DriverLB.setVisible(true);
        }
        else{
            errorLB.setVisible(true);
        }
    }
   

}

