package com.gn.module.vehicles;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;

public class VehiclesController implements Initializable{

    @FXML
    private JFXTextField regnoTF, brandTF, modelTF, colorTF, searchTF;
    @FXML
    private JFXComboBox <String> driverCB, typeCB;
    @FXML
    private Label errorLB, regnoLB, brandLB, modelLB, colorLB, typeLB, driverLB;
    @FXML
    private HBox regnoBOX, brandBOX, modelBOX, colorBOX, typeBOX, driverBOX;
    
      @FXML
    private JFXTreeTableView<VehiclesModel> vehiclesTable;
     
      
String vehicle_id, vehicle_regNo, vehicle_brand, vehicle_model, vehicle_color, vehicle_type , driver_id ;
    
    ObservableList<VehiclesModel> VehiclesList;
    
    
    private ObservableList<ObservableList> vehiclesData;
    
  ObservableList<String> DriversData= FXCollections.observableArrayList();
  
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static  String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs= null;
    
    private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    

    }
    //load the driver name on the combo box
              public void getDriverName(){
                 try {
                     String sql="SELECT * from milk_stock.milk_staff where staff_role = 'driver' ";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         DriversData.add(rs.getString("staff_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
       @Override
    public void initialize(URL location, ResourceBundle resources) {
        addEffect(regnoTF);
        addEffect(brandTF);
        addEffect(modelTF);
        addEffect(colorTF);
        addEffect(typeCB);
        addEffect(driverCB);
                
        typeCB.getItems().addAll("PickUp", "Lorry");
        getDriverName();
            driverCB.setItems(DriversData);
        
         Mask.nameField(brandTF);
         Mask.nameField(modelTF);
         Mask.nameField(colorTF);
         
        setupListeners();
        
        JFXTreeTableColumn<VehiclesModel, String> ID = new JFXTreeTableColumn<>("Vehicle Id");

             ID.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_id;

            }
        });
        JFXTreeTableColumn<VehiclesModel, String> RegNo = new JFXTreeTableColumn<>("Vehicle Reg. Number");

             RegNo.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_regNo;

            }
        });
        JFXTreeTableColumn<VehiclesModel, String> Brand = new JFXTreeTableColumn<>("Vehicle Brand Name");

             Brand.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_brand;

            }
        });
        JFXTreeTableColumn<VehiclesModel, String> Model = new JFXTreeTableColumn<>("Vehicle Model Name");

            Model.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_model;

            }
        });
        JFXTreeTableColumn<VehiclesModel, String> Color = new JFXTreeTableColumn<>("Vehicle Color");

            Color.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_color;

            }
        });
        JFXTreeTableColumn<VehiclesModel, String> Type = new JFXTreeTableColumn<>("Vehicle Type");

             Type.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().vehicle_type;

            }
        });    
        JFXTreeTableColumn<VehiclesModel, String> Driver = new JFXTreeTableColumn<>("Driver Name");

             Driver.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<VehiclesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<VehiclesModel, String> param) {
                return param.getValue().getValue().driver_id;

            }
        });    
         VehiclesList = FXCollections.observableArrayList();  
          
        TreeItem<VehiclesModel> root = new RecursiveTreeItem<VehiclesModel>(VehiclesList, RecursiveTreeObject::getChildren);
        vehiclesTable.getColumns().addAll(ID, RegNo, Brand, Model, Color, Type, Driver);
        vehiclesTable.setRoot(root);
        vehiclesTable.setShowRoot(false);
             
        addrowsToTable();
        
             searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                        vehiclesTable.setPredicate(new Predicate<TreeItem<VehiclesModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<VehiclesModel> t) {
                               
                               boolean flag = t.getValue().vehicle_id.getValue().contains(newValue)
                                       || t.getValue().vehicle_regNo.getValue().contains(newValue)
                                       || t.getValue().vehicle_brand.getValue().contains(newValue)
                                       || t.getValue().vehicle_model.getValue().contains(newValue)
                                       || t.getValue().vehicle_color.getValue().contains(newValue)
                                       || t.getValue().vehicle_type.getValue().contains(newValue)
                                       || t.getValue().driver_id.getValue().contains(newValue);
                               return flag;
                               
                           }
                       });
                   }
                           
               });

    }
    
    private boolean validRegNo(){
        return Mask.noSymbols(regnoTF) && !regnoTF.getText().isEmpty() && regnoTF.getLength() > 3  ;

    }
    private boolean validBrandName(){
        return Mask.nameField(brandTF) && Mask.noSymbols(brandTF) && !brandTF.getText().isEmpty() && brandTF.getLength() > 3 ;
    }

    private boolean validModelName(){
        return  Mask.nameField(modelTF) && Mask.noSymbols(modelTF) && !modelTF.getText().isEmpty() && modelTF.getLength() > 3 ;
    }
    private boolean validColor(){
        return Mask.nameField(colorTF) && Mask.noSymbols(colorTF) && !colorTF.getText().isEmpty() && colorTF.getLength() > 3  ;

    }
    private boolean validType(){
        return !typeCB.getItems().isEmpty(); 
    }

    private boolean validDriver(){
        return  !driverCB.getItems().isEmpty() ;
    }
    
    private void setupListeners(){
        regnoTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRegNo()){
                if(!newValue){
                    Flash swing = new Flash(regnoBOX);
                    regnoLB.setVisible(true);
                    new SlideInLeft(regnoLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    regnoBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    regnoLB.setVisible(false);
                }
            } else {
              errorLB.setVisible(false);
            }
        });
        brandTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validBrandName()){
                if(!newValue){
                    Flash swing = new Flash(brandBOX);
                    brandLB.setVisible(true);
                    new SlideInLeft(brandLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    brandBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    brandLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(false);
            }
        });
        modelTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validModelName()){
                if(!newValue){
                    Flash swing = new Flash(modelBOX);
                    modelLB.setVisible(true);
                    new SlideInLeft(modelLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    modelBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    modelLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(true);
            }
        });
  
        colorTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validColor()){
                if(!newValue){
                    Flash swing = new Flash(colorBOX);
                    colorLB.setVisible(true);
                    new SlideInLeft(colorLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    colorBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    colorLB.setVisible(false);
                }
            }  else {
               errorLB.setVisible(true);
            }
        });

        typeCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validType()){
                if(!newValue){
                    Flash swing = new Flash(typeBOX);
                    typeLB.setVisible(true);
                    new SlideInLeft(typeLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    typeBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    typeLB.setVisible(false);
                }
            }  else {
            errorLB.setVisible(true);
            }
        });
        driverCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validDriver()){
                if(!newValue){
                    Flash swing = new Flash(driverBOX);
                    driverLB.setVisible(true);
                    new SlideInLeft(driverLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    driverBOX.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    driverLB.setVisible(false);
                }
            }  else {
             errorLB.setVisible(true);
            }
        });
        }
    @FXML
    private void clearFields(){
        regnoTF.clear();
        brandTF.clear();
        modelTF.clear();
        colorTF.clear();
    }
    @FXML
    private void AddVehicle(ActionEvent event){
        if(validRegNo() && validBrandName() && validModelName()&& validColor() && validType() && validDriver()){
            insert(regnoTF.getText(), brandTF.getText(), modelTF.getText(), colorTF.getText(), typeCB.getValue(), driverCB.getValue() );
        }else if (!validRegNo()){
            regnoLB.setVisible(true);
        } else if (!validBrandName()) {
           brandLB.setVisible(true);
        } else if(!validModelName()){
            modelLB.setVisible(true);
        } else if(!validColor()){
            colorLB.setVisible(true);
        }else if(!validType()){
            typeLB.setVisible(true);
        }else if(!validDriver()){
            driverLB.setVisible(true);
        }
        else{
            errorLB.setVisible(true);
        }
    }
    private void insert(String vehicle_regNo, String vehicle_brand, String vehicle_model,String vehicle_color ,String vehicle_type, String driver_id){
        sqlInsert= "INSERT INTO milk_stock.milk_vehicles(vehicle_regNo, vehicle_brand, vehicle_model, vehicle_color, vehicle_type, driver_id) VALUES(?,?,?,?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, vehicle_regNo);
            stat.setString(2, vehicle_brand);
            stat.setString(3, vehicle_model);
            stat.setString(4, vehicle_color);
            stat.setString(5, vehicle_type);
            stat.setString(6, driver_id);
            stat.executeUpdate();
            
         
             Alerts.success("Success", vehicle_regNo + " - added succesfully"); 
  
            clearFields();
   
        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
}
     void addrowsToTable() {

        String sqlSelect = "SELECT vehicle_id, vehicle_regNo, vehicle_brand, vehicle_model, vehicle_color, vehicle_type , driver_id FROM milk_stock.milk_vehicles";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                VehiclesList.add(new  VehiclesModel(result.getString(1), result.getString(2), result.getString(3),result.getString(4), result.getString(5), result.getString(6), result.getString(7)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }
    public void refreshTable(){
          VehiclesList.clear();
             addrowsToTable();
      }
   
    @FXML
    public void editVehicle(ActionEvent event){
        try {
            TreeItem <VehiclesModel> vehicle = (TreeItem<VehiclesModel>)vehiclesTable.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/com/gn/module/vehicles/editVehicle.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        
        Stage stage = new Stage();
        //BoxBlur = new BoxBlur(3,3,3);
        stage.setScene(new Scene(p));
        //Creates a modal over the primary stage without adding a view on the task bar
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
        
         // Get controller of card 
          EditVehicleController card = loader.<EditVehicleController>getController();
            //Pass  data  
          card.transferData( vehicle.getValue().getVehicle_id() , vehicle.getValue().getVehicle_regNo(), vehicle.getValue().getVehicle_brand(), vehicle.getValue().getVehicle_model(), vehicle.getValue().getVehicle_color(), vehicle.getValue().getVehicle_type() , vehicle.getValue().getDriver_id());
        stage.show();
        } catch (IOException ex) {
            Alerts.info("Error", ex.getMessage());
        }catch(NullPointerException e){
            Alerts.info("Info", "Select vehicle details you want to update ");
        }
    }
@FXML
    public void deleteVehicle(){
        try {
            TreeItem <VehiclesModel> vehicle = (TreeItem<VehiclesModel>)vehiclesTable.getSelectionModel().getSelectedItem();
            
            String sql = " DELETE FROM milk_stock.milk_vehicles WHERE vehicle_id ='" + vehicle.getValue().getVehicle_id() + "'";
            
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            stat.executeUpdate();
            
            refreshTable();
            
            Alerts.info("Info", " Vehicle deleted Successfully");
            
    }catch(NullPointerException m){
        Alerts.info("Info", "Select the vehicle you want to delete");
    }
     catch (SQLException r) {
        Alerts.error("Error", r.getMessage());
    } catch (ClassNotFoundException n) {
       Alerts.error("Error",n.getMessage());
}
}
}