
package com.gn.module.logs;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LogsModel extends RecursiveTreeObject<LogsModel> {
    
      public StringProperty  log_id, username, time_in, time_out;  
      
      public LogsModel (String log_id, String username, String time_in, String time_out){
             this.log_id=new SimpleStringProperty(log_id); 
             this.username=new SimpleStringProperty(username);
             this.time_in=new SimpleStringProperty(time_in); 
             this.time_out=new SimpleStringProperty(time_out); 
      }

    public String getLog_id() {
        return log_id.get();
    }

    public void setLog_id(String log_id) {
           this.log_id = new SimpleStringProperty(log_id);
    }

    public String getUsername() {
        return username.get();
    }

    public void setUsername(String username) {
          this.username = new SimpleStringProperty(username);
    }

    public String getTime_in() {
        return time_in.get();
    }

    public void setTime_in(String time_in) {
           this.time_in = new SimpleStringProperty(time_in);
    }

    public String getTime_out() {
        return time_out.get();
    }

    public void setTime_out(String time_out) {
           this.time_out = new SimpleStringProperty(time_out);
    }

      
}

