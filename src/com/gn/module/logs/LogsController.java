
package com.gn.module.logs;

import com.gn.global.util.Alerts;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;


public class LogsController implements Initializable {
    @FXML
    private JFXTextField searchTF;
    @FXML
    private JFXTreeTableView<LogsModel> logsTableView;
    
     public static ObservableList<LogsModel> LogsList;
     
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306/milk_stock";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
   static  ResultSet result;
   static  ResultSet rs=null;
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      JFXTreeTableColumn<LogsModel, String> Id = new JFXTreeTableColumn<>("Log ID");     
             Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<LogsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<LogsModel, String> param) {
                       return param.getValue().getValue().log_id;
                       
                   }
         });
      JFXTreeTableColumn<LogsModel, String> User = new JFXTreeTableColumn<>("Username");     
             User.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<LogsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<LogsModel, String> param) {
                       return param.getValue().getValue().username;
                       
                   }
         });              
       JFXTreeTableColumn<LogsModel, String> TimeIn = new JFXTreeTableColumn<>(" Login Time");     
             TimeIn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<LogsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<LogsModel, String> param) {
                       return param.getValue().getValue().time_in;
                       
                   }
         }); 
        JFXTreeTableColumn<LogsModel, String> TimeOut = new JFXTreeTableColumn<>("LogOut Time");     
             TimeOut.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<LogsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<LogsModel, String> param) {
                       return param.getValue().getValue().time_out;
                       
                   }
         });            
         LogsList = FXCollections.observableArrayList();
               
               TreeItem<LogsModel> root = new RecursiveTreeItem<LogsModel>(LogsList, RecursiveTreeObject::getChildren);
              
               logsTableView.getColumns().addAll( Id, User, TimeIn, TimeOut);
               logsTableView.setRoot(root);
               logsTableView.setShowRoot(false);   
               
                         addrowsToTable();
               
               
               searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                       logsTableView.setPredicate(new Predicate<TreeItem<LogsModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<LogsModel> t) {
                               
                               boolean flag = t.getValue().log_id.getValue().contains(newValue)
                                       || t.getValue().username.getValue().contains(newValue)
                                       || t.getValue().time_in.getValue().contains(newValue);
                                    //   || t.getValue().time_out.getValue().contains(newValue);
                                      
                               return flag;
                               
                           }
                       });
                   }
                           
               });     
    }    
       public static void addrowsToTable() {

        String sqlSelect = "SELECT * FROM milk_stock.login_logs";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                LogsList.add(new LogsModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }

    @FXML
    public void refreshTable (){
        refresh();
    }
    public static void refresh(){
          LogsList.clear();
            addrowsToTable();
      }
   public void  print(){
       
String query ="SELECT login_logs.`log_id` AS login_logs_log_id, login_logs.`username` AS login_logs_username, login_logs.`login_time` AS login_logs_login_time, login_logs.`logout_time` AS login_logs_logout_time FROM `login_logs` login_logs";
         
           try {
               
             Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(query);

            result = stat.executeQuery();
            
            JasperDesign jdesign = JRXmlLoader.load("F:\\alex\\MilkStock\\src\\com\\gn\\module\\reports\\report1.jrxml");
          
            JRDesignQuery updateQuery = new JRDesignQuery();
            updateQuery.setText(query);

            jdesign.setQuery(updateQuery);

            JasperReport jreport = JasperCompileManager.compileReport(jdesign);
            JasperPrint jprint = JasperFillManager.fillReport(jreport, null, conn);
            JasperViewer.viewReport(jprint);

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (JRException ex) {
            ex.printStackTrace();
        }
   }
}
