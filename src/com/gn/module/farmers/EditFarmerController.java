package com.gn.module.farmers;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.GNAvatarView;
import static com.gn.global.util.AddEffect.addEffect;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class EditFarmerController implements Initializable {
    @FXML private HBox box_fullname, box_email, box_contact, box_id, box_route, box_stage, box_account_number;
    @FXML private JFXTextField  txtfullname, txtemail, txtcontact, txtid, accountTF, searchTF;
    @FXML private Label  NumberLB, lbl_fullname, lbl_id, lbl_email, lbl_contact,lbl_account, lbl_error,lbl_stage;
    @FXML private GNAvatarView imageView;
    @FXML private JFXComboBox<String> comboStage;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs=null;

 ObservableList<String> data= FXCollections.observableArrayList();    
    
    @FXML
    private void Close(ActionEvent event){
       Button btn = (Button) event.getSource();
      Stage stage =(Stage) btn.getScene().getWindow();
       stage.close();
       
    }

    private void setupListeners(){
        txtfullname.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validFullName()){
                if(!newValue){
                    Flash swing = new Flash(box_fullname);
                    lbl_fullname.setVisible(true);
                    new SlideInLeft(lbl_fullname).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_fullname.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_fullname.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        txtemail.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_email);
                    lbl_email.setVisible(true);
                    new SlideInLeft(lbl_email).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_email.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_email.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(false);
            }
        });
        txtcontact.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_contact);
                    lbl_contact.setVisible(true);
                    new SlideInLeft(lbl_contact).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_contact.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_contact.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
  
        accountTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_account_number);
                    lbl_account.setVisible(true);
                    new SlideInLeft(lbl_account).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_account_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_account.setVisible(false);
                }
            }  else {
               lbl_error.setVisible(true);
            }
        });

        txtid.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validID()){
                if(!newValue){
                    Flash swing = new Flash(box_id);
                    lbl_id.setVisible(true);
                    new SlideInLeft(lbl_id).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_id.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_id.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        comboStage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStation()){
                if(!newValue){
                    Flash swing = new Flash(box_stage);
                    lbl_stage.setVisible(true);
                    new SlideInLeft(lbl_stage).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_stage.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_stage.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        }
        
        

            private boolean validFullName(){
                    return Mask.noSymbols(txtfullname) && !txtfullname.getText().isEmpty() && txtfullname.getLength() > 3  ;

                }
            private boolean validEmail(){
                    return Mask.isEmail(txtemail);
            }

            private boolean validContact(){
                    return Mask.noLetters(txtcontact) && Mask.noSpaces(txtcontact) && Mask.noSymbols(txtcontact) && !txtcontact.getText().isEmpty() && txtcontact.getLength() > 3 ;
                }
            private boolean validID(){
                    return Mask.noLetters(txtid) && Mask.noSpaces(txtid) && Mask.noSymbols(txtid) && !txtid.getText().isEmpty() && txtid.getLength() > 3 ;
                }
            private boolean validAccount(){
                    return Mask.noLetters(accountTF) && Mask.noSpaces(accountTF) && Mask.noInitSpace(accountTF) && Mask.noSymbols(accountTF) && !accountTF.getText().isEmpty() && accountTF.getLength() > 3 ;
                }
            private boolean validStation(){
                return !comboStage.getSelectionModel().isEmpty();
            } 
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
      setupListeners();
      
               addEffect(txtfullname);
               addEffect(txtemail);
               addEffect(txtcontact);
               addEffect(txtid);
               addEffect(accountTF);
               //addEffect(comboRoute);
               addEffect(comboStage);
               
              getStageName();
             comboStage.setItems(data);
    }
    void transferData(String Number, String Name, String Email, String Contact, String Id,String Account, String Station) {
       NumberLB.setText(Number);
       txtfullname.setText(Name);
       txtemail.setText(Email);
       txtcontact.setText(Contact);
       txtid.setText(Id);
       accountTF.setText(Account);
       comboStage.setValue(Station);
       
    }
    
    @FXML
    private void UpdateDetail(ActionEvent event){
        if(validFullName() && validEmail() && validContact() && validID() && validAccount() && validStation() ){
       
        update();
        
        Button btn = (Button) event.getSource();
        Stage stage = (Stage) btn.getScene().getWindow();
        stage.close();
        
       //FarmersList.add(new FarmersModel(imageView.getContent(), txtnumber.getText(), txtfullname.getText(), txtemail.getText(), txtcontact.getText(), txtid.getText(), accountTF.getText(), comboStage.getValue()));
    } else if (!validFullName()){
            lbl_fullname.setVisible(true);
        } else if (!validEmail()) {
            lbl_email.setVisible(true);
        } else if(!validContact()){
            lbl_contact.setVisible(true);
        } else if(!validID()){
            lbl_id.setVisible(true);
        }else if(!validAccount()){
            lbl_account.setVisible(true);
        }else if(!validStation()){
            lbl_stage.setVisible(true);
        }
        else{
            lbl_error.setVisible(true);
        }
   
    }
    private void update(){
     String status="Active";
       
        String sqlUpdate = "UPDATE  milk_stock.milk_farmers SET farmer_name='"+ txtfullname.getText() + "' ,farmer_email='" + txtemail.getText() + "' , farmer_contact='" + txtcontact.getText()+ "', farmer_id ='" + txtid.getText()+  "', farmer_account_number ='" + accountTF.getText()+  "', station_id ='" + comboStage.getValue()+  "',  status ='" + status +  "' WHERE farmer_number='" + NumberLB.getText() + "'";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlUpdate);
            stat.executeUpdate();
            

            staffFarmersController.refresh();
             Alerts.success("Success", "Record for '"+ txtfullname.getText() + "' - updated succesfully"); 
             
           

        
   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }

    }
  
       public void getStageName(){
            try {
                     String sql="SELECT * from milk_stock.milk_stations";
       
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         data.add(rs.getString("station_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }

}