/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gn.module.farmers;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.GNAvatarView;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import static com.gn.module.farmers.FarmersController.rs;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import javax.imageio.ImageIO;

/**
 *
 * @author Alex
 */
public class StaffAddFarmer implements Initializable{
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
   static  ResultSet result;
   static  ResultSet rs=null;
   
    ObservableList<String> data= FXCollections.observableArrayList();
     public static ObservableList<FarmersModel> FarmersList;
      
    @FXML private HBox box_fullname, box_email, box_contact, box_id, box_route, box_stage, box_account_number;
    @FXML private JFXTextField txtnumber, txtfullname, txtemail, txtcontact, txtid, accountTF, searchTF;
    @FXML private Label  lbl_fullname, lbl_id, lbl_email, lbl_contact,lbl_account, lbl_error,lbl_stage;
    @FXML private GNAvatarView imageView;
    @FXML private JFXComboBox<String> comboStage;
    
     private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    

    }
     
        private void setupListeners(){

        txtfullname.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validFullName()){
                if(!newValue){
                    Flash swing = new Flash(box_fullname);
                    lbl_fullname.setVisible(true);
                    new SlideInLeft(lbl_fullname).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_fullname.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_fullname.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        txtemail.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_email);
                    lbl_email.setVisible(true);
                    new SlideInLeft(lbl_email).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_email.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_email.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(false);
            }
        });
        txtcontact.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_contact);
                    lbl_contact.setVisible(true);
                    new SlideInLeft(lbl_contact).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_contact.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_contact.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
  
        accountTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_account_number);
                    lbl_account.setVisible(true);
                    new SlideInLeft(lbl_account).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_account_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_account.setVisible(false);
                }
            }  else {
               lbl_error.setVisible(true);
            }
        });

        txtid.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validID()){
                if(!newValue){
                    Flash swing = new Flash(box_id);
                    lbl_id.setVisible(true);
                    new SlideInLeft(lbl_id).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_id.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_id.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        comboStage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStation()){
                if(!newValue){
                    Flash swing = new Flash(box_stage);
                    lbl_stage.setVisible(true);
                    new SlideInLeft(lbl_stage).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_stage.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_stage.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        }
        
            private boolean validFullName(){
                    return Mask.noSymbols(txtfullname) && !txtfullname.getText().isEmpty() && txtfullname.getLength() > 3  ;

                }
            private boolean validEmail(){
                    return Mask.isEmail(txtemail);
            }

            private boolean validContact(){
                    return Mask.noLetters(txtcontact) && Mask.noSpaces(txtcontact) && Mask.noSymbols(txtcontact) && !txtcontact.getText().isEmpty() && txtcontact.getLength() > 3 ;
                }
            private boolean validID(){
                    return Mask.noLetters(txtid) && Mask.noSpaces(txtid) && Mask.noSymbols(txtid) && !txtid.getText().isEmpty() && txtid.getLength() == 8 ;
                }
            private boolean validAccount(){
                    return Mask.noLetters(accountTF) && Mask.noSpaces(accountTF) && Mask.noInitSpace(accountTF) && Mask.noSymbols(accountTF) && !accountTF.getText().isEmpty() && accountTF.getLength() > 3 ;
                }
            private boolean validStation(){
                return !comboStage.getSelectionModel().isEmpty();
            }
    @FXML 
    private void clearFields(){
        txtfullname.clear();
        txtemail.clear();
        txtcontact.clear();
        txtid.clear();
        accountTF.clear();
        
    }
       public void getStageName(){
            try {
                     String sql="SELECT * from milk_stock.milk_stations";
       
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         data.add(rs.getString("station_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
 
    @Override
    public void initialize(URL location, ResourceBundle resources) {
                addEffect(txtfullname);
               addEffect(txtemail);
               addEffect(txtcontact);
               addEffect(txtid);
               addEffect(accountTF);
               addEffect(comboStage);
               
                  getStageName();
                comboStage.setItems(data);
               setupListeners();
    }
 @FXML
void addFarmer(ActionEvent event){
    if(validFullName() && validEmail() && validContact() && validID() && validAccount() && validStation() ){
        String status="Active";
        insert(txtfullname.getText(), txtemail.getText(), txtcontact.getText(), txtid.getText(), accountTF.getText(), comboStage.getValue(), status);
       FarmersList.add(new FarmersModel( txtnumber.getText(), txtfullname.getText(), txtemail.getText(), txtcontact.getText(), txtid.getText(), accountTF.getText(), comboStage.getValue(), status));
    } else if (!validFullName()){
            lbl_fullname.setVisible(true);
        } else if (!validEmail()) {
            lbl_email.setVisible(true);
        } else if(!validContact()){
            lbl_contact.setVisible(true);
        } else if(!validID()){
            lbl_id.setVisible(true);
        }else if(!validAccount()){
            lbl_account.setVisible(true);
        }else if(!validStation()){
            lbl_stage.setVisible(true);
        }
        else{
            lbl_error.setVisible(true);
        }
}
private void insert(String farmer_name, String farmer_email, String farmer_contact, String farmer_id, String farmer_account, String farmer_station_id, String status){
    
        sqlInsert= "INSERT INTO milk_stock.milk_farmers( farmer_name, farmer_email, farmer_contact, farmer_id, farmer_account_number, station_id, status) VALUES(?,?,?,?,?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, farmer_name);
            stat.setString(2, farmer_email);
            stat.setString(3, farmer_contact);
            stat.setString(4, farmer_id);
            stat.setString(5, farmer_account);
            stat.setString(6, farmer_station_id);
            stat.setString(7, status);
            stat.executeUpdate();
            
         
             Alerts.success("Success", farmer_name + " - added succesfully"); 
             
          
             
            clearFields();
   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
}
 public void selectImage() {
         FileChooser imageFC= new FileChooser();
           //Set Extension filters
           FileChooser.ExtensionFilter extFilterJPG= new FileChooser.ExtensionFilter("JPG Files(*.JPG)", "*.JPG");
         FileChooser.ExtensionFilter extFilterjpg= new FileChooser.ExtensionFilter("jpg Files(*.jpg)", "*.jpg");
         FileChooser.ExtensionFilter extFilterPNG= new FileChooser.ExtensionFilter("PNG Files(*.PNG)", "*.PNG");
         FileChooser.ExtensionFilter extFilterpng= new FileChooser.ExtensionFilter("JPG Files(*.png)", "*.png");
         imageFC.getExtensionFilters().addAll(extFilterJPG, extFilterjpg,extFilterPNG, extFilterpng);
     
         File file= imageFC.showOpenDialog(null);
         try{
             BufferedImage bufferedImage = ImageIO.read(file);
             Image image=SwingFXUtils.toFXImage(bufferedImage, null);
             
             imageView.setImage(image);
         }catch(IOException ex){
             Alerts.info("Error", ex.getMessage());
         }
     }
}
