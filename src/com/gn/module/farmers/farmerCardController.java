
package com.gn.module.farmers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;


public class farmerCardController implements Initializable{
    @FXML
    private Label lbName;

    @FXML
    private Label lbEmail;

    @FXML
    private Label lbContact;

    @FXML
    private Label lbRoute, lbAccount;

    @FXML
    private Label lbStation, lbNumber;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
     //transferData(String Name, String Email, String Contact, String Route, String Station); 
   
    }
 
      void transferData(String Number, String Name, String Email, String Contact, String Id,String Account, String Station){
       lbNumber.setText(Number);
        lbName.setText(Name);
       lbEmail.setText(Email);
       lbContact.setText(Contact);
       lbAccount.setText(Account);
       lbStation.setText(Station);
       
    }
    
}
