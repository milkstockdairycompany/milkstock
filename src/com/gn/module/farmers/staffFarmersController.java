
package com.gn.module.farmers;

import com.gn.global.util.Alerts;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.FileInputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.stage.Window;
import javafx.util.Callback;

    import com.hubtel.ApiHost;
    import com.hubtel.BasicAuth;
    import com.hubtel.HttpRequestException;
    import com.hubtel.MessageResponse;
    import com.hubtel.MessagingApi;


public class staffFarmersController implements Initializable {
       @FXML private JFXTreeTableView<FarmersModel> farmersTableView;
 @FXML private JFXTextField searchTF;

    
   ObservableList<String> data= FXCollections.observableArrayList();
   
   
//private final PopOver pop = new PopOver;
        
    private FileInputStream fis;
            
 
        
        
   
   public static ObservableList<FarmersModel> FarmersList;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
   
   static  ResultSet result;
   static  ResultSet rs=null;


     
   

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        JFXTreeTableColumn<FarmersModel, String> FNumber= new JFXTreeTableColumn<>("Farmer Number");
               
             FNumber.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_number;
                       
                   }
         });
               
               JFXTreeTableColumn<FarmersModel, String> FName = new JFXTreeTableColumn<>(" Name");
               
               FName.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_name;
                       
                   }
               });
               
               JFXTreeTableColumn<FarmersModel, String> Email = new JFXTreeTableColumn<>(" Email");
               
               Email.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_email;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Contact = new JFXTreeTableColumn<>("Contact");
               
               Contact.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_contact;
                       
                   }
               });
               
               JFXTreeTableColumn<FarmersModel, String> Id = new JFXTreeTableColumn<>("Farmer Id");
               
               Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_id;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Account = new JFXTreeTableColumn<>("Account Number");
               
               Account.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_account;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Stage = new JFXTreeTableColumn<>("Station");
               
               Stage.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_station_id;
                       
                   }
               });
              JFXTreeTableColumn<FarmersModel, String> Status = new JFXTreeTableColumn<>("Status");
               
               Status.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().status;
                       
                   }
               });
               
               FarmersList = FXCollections.observableArrayList();
               
               TreeItem<FarmersModel> root = new RecursiveTreeItem<FarmersModel>(FarmersList, RecursiveTreeObject::getChildren);
              
               farmersTableView.getColumns().addAll(FNumber, FName, Email, Contact, Id, Account, Stage, Status);
               farmersTableView.setRoot(root);
               farmersTableView.setShowRoot(false);
               
               addrowsToTable();
               
               
               searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                       farmersTableView.setPredicate(new Predicate<TreeItem<FarmersModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<FarmersModel> t) {
                               
                               boolean flag = t.getValue().farmer_name.getValue().contains(newValue)
                                       || t.getValue().farmer_email.getValue().contains(newValue)
                                       || t.getValue().farmer_contact.getValue().contains(newValue)
                                       || t.getValue().farmer_id.getValue().contains(newValue)
                                       || t.getValue().farmer_account.getValue().contains(newValue)
                                       || t.getValue().farmer_station_id.getValue().contains(newValue)
                                       || t.getValue().status.getValue().contains(newValue);
                               return flag;
                               
                           }
                       });
                   }
                           
               });

                
    }

    


 
            

     static void addrowsToTable() {

        String sqlSelect = "SELECT  farmer_number, farmer_name, farmer_email, farmer_contact, farmer_id, farmer_account_number, station_id, status FROM milk_stock.milk_farmers";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                FarmersList.add(new FarmersModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5), result.getString(6), result.getString(7), result.getString(8)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }

    @FXML
    public void refreshTable (){
        refresh();
    }
    public static void refresh(){
          FarmersList.clear();
            addrowsToTable();
      }
      
      @FXML
      void messageFarmer(ActionEvent event){
        BasicAuth auth = new BasicAuth("bkgimgua", "pzuhxqzg");
            ApiHost host = new ApiHost(auth);
            // Instance of the Messaging API
            MessagingApi messagingApi = new MessagingApi(host);

            try {
                MessageResponse response = messagingApi.sendQuickMessage("+254790317338", "+254745344290", "MilkStock Test Sms ", "0790317338");

                Alerts.error("Server Error", "Server Response Status " + response.getStatus());
            } catch (HttpRequestException ex) {
                Alerts.error("Error", "Exception Server Response Status " + ex.getHttpResponse().getStatus());
                Alerts.error("Error", "Exception Server Response Body " + ex.getHttpResponse().getBodyAsString());
            }

        }
      
     public void print(){
      try{
         TreeItem<FarmersModel> farmer = (TreeItem<FarmersModel >) farmersTableView.getSelectionModel().getSelectedItem();
                FXMLLoader loader= new FXMLLoader();
                     loader.setLocation(getClass().getResource("/com/gn/module/farmers/farmerCard.fxml"));
                   
                         loader.load();
            //Get controller of card 
            farmerCardController card = loader.<farmerCardController>getController();
            //Pass  data  
//          card.transferData(Number, Name,Email, Contact, Route, Station);
                   card.transferData(farmer.getValue().getFarmer_number(), farmer.getValue().getFarmer_name(), farmer.getValue().getFarmer_email(), farmer.getValue().getFarmer_contact(),farmer.getValue().getFarmer_id() ,farmer.getValue().getFarmer_account() , farmer.getValue().getFarmer_station_id());
        

            Parent root= loader.getRoot();
             Window window=null;
             PrinterJob job = PrinterJob.createPrinterJob();
             if(job != null){
                 job.showPrintDialog(window); 
                  job.printPage(root);
              job.endJob();
     }
      
     }catch(Exception e){
         Alerts.info("Info", "Select the farmer details you want to print. ");
     }
     
     } 
    
 

    
 
                     
                 
}

 



