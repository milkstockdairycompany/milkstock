
package com.gn.module.farmers;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.ImageView;

public class FarmersModel extends RecursiveTreeObject<FarmersModel> {

  
     public   StringProperty farmer_number, farmer_name, farmer_email, farmer_contact, farmer_id, farmer_account , farmer_station_id , status;
  
      
      public FarmersModel( String farmer_number, String  farmer_name,String farmer_email, String farmer_contact,String farmer_id, String farmer_account , String farmer_station_id, String status){
         
          this.farmer_number = new SimpleStringProperty(farmer_number);
          this.farmer_name = new SimpleStringProperty(farmer_name);
          this.farmer_email = new SimpleStringProperty(farmer_email);
          this.farmer_contact = new SimpleStringProperty(farmer_contact);
          this.farmer_id =new SimpleStringProperty(farmer_id);
          this.farmer_account = new SimpleStringProperty(farmer_account);
          this.farmer_station_id = new SimpleStringProperty(farmer_station_id);
          this.status=new SimpleStringProperty(status);
      }

  
 public void setFarmer_number(String farmer_number) {
        this.farmer_number = new SimpleStringProperty(farmer_number);
    }
  
    public void setFarmer_name(String farmer_name) {
        this.farmer_name = new SimpleStringProperty(farmer_name);
    }
    public void setFarmer_email(String farmer_email) {
        this.farmer_email = new SimpleStringProperty (farmer_email);
    }
    public void setFarmer_contact(String farmer_contact) {
        this.farmer_contact = new SimpleStringProperty (farmer_contact);
    }
    public void setFarmer_id(String farmer_id) {
        this.farmer_id = new SimpleStringProperty (farmer_id);
    }
    public void setFarmer_account(String farmer_account) {
        this.farmer_account = new SimpleStringProperty (farmer_account);
    }
    public void setFarmer_station_id(String farmer_station_id) {
        this.farmer_station_id = new SimpleStringProperty (farmer_station_id);
    }
    public void setStatus(String status) {
        this.status= new SimpleStringProperty (status);
    }
 
 public String getFarmer_number() {
        return  farmer_number.get();
    }

    public String getFarmer_name() {
        return farmer_name.get();
    }
    public String getFarmer_email() {
        return farmer_email.get();
    }
    public String getFarmer_contact() {
        return farmer_contact.get();
    }
    public String getFarmer_id() {
        return farmer_id.get();
    }

  public String getFarmer_account() {
        return farmer_account.get();
    }
    public String getFarmer_station_id() {
        return farmer_station_id.get();
    }
      public String getStatus() {
        return status.get();
    }   
    
}
