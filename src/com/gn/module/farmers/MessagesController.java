/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gn.module.farmers;

import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import com.jfoenix.controls.*;
import java.util.Date;
import java.util.Properties;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;

public class MessagesController  {

    @FXML
    private JFXTextField emailTF,subjectTF;

    @FXML
    private JFXTextArea messageTF;
    
    @FXML
    private void checkMail(ActionEvent event){
        if(ValidEmail() && ValidSubject() && ValidMessage()){
            
           EmailFarmer(); 
           
           clearFields();
        }else{
            Alerts.error("Errror","Check for error");
        }
    }

     void EmailFarmer(){
          try{
            String host ="smtp.gmail.com" ;
            String user = "milkstockdairycompany@gmail.com";
            String pass = "maziwamilk";
            String to = emailTF.getText();
            String from = "milkdairystockcompany@gmail.com";
            String subject = subjectTF.getText();
            String messageText = messageTF.getText();
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "465");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           
           System.out.println("message send successfully");
                Notifications  notification=Notifications.create()
                    // .graphic(new ImageView(image))
                    .title("Message Sent")
                    .text(" Email Sent")
                    .hideAfter(Duration.seconds(30))
                    .position(Pos.BOTTOM_RIGHT);                           
                    notification.showInformation();
        }catch(Exception ex)
        {
            System.out.println(ex);
                Alerts.error("Error", ex.getMessage());   
    }

     }
private void clearFields(){
    emailTF.clear();
    subjectTF.clear();
    messageTF.clear();
}

@FXML 
private void clear(){
    clearFields();
}
     
    @FXML
    private void info(){
        Alerts.info("Info", "Lorem ipsum dolor color.");
    }

    @FXML
    private void warning(){
        Alerts.warning("Warning", "Lorem ipsum dolor color.");
    }

    @FXML
    private void success(){
        Alerts.success("Success", "Lorem ipsum dolor color.");
    }

    @FXML
    private void error(){
        Alerts.error("Error", "Lorem ipsum dolor color.");
    }
    
    
    private boolean ValidEmail(){
       return  Mask.isEmail(emailTF);
    }
    
     private boolean ValidSubject(){
       return !subjectTF.getText().isEmpty() && subjectTF.getLength()>3;
    }
    private boolean ValidMessage(){
       return !messageTF.getText().isEmpty() && messageTF.getLength()>3;
    } 
}





