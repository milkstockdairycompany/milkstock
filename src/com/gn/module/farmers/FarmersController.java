
package com.gn.module.farmers;

import animatefx.animation.Flash;

import animatefx.animation.SlideInLeft;
import com.gn.GNAvatarView;
import static com.gn.global.util.AddEffect.addEffect;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.imageio.ImageIO;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage;
import org.controlsfx.control.Notifications;

    import com.hubtel.ApiHost;
    import com.hubtel.BasicAuth;
    import com.hubtel.HttpRequestException;
    import com.hubtel.MessageResponse;
    import com.hubtel.MessagingApi;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

public class FarmersController implements Initializable {
       @FXML private JFXTreeTableView<FarmersModel> farmersTableView;

    @FXML private HBox box_fullname, box_email, box_contact, box_id, box_route, box_stage, box_account_number;
    @FXML private JFXTextField txtnumber, txtfullname, txtemail, txtcontact, txtid, accountTF, searchTF;
    @FXML private Label  lbl_fullname, lbl_id, lbl_email, lbl_contact,lbl_account, lbl_error,lbl_stage;
    @FXML private GNAvatarView imageView;
    @FXML private JFXComboBox<String> comboStage;
    
   ObservableList<String> data= FXCollections.observableArrayList();
   
   
//private final PopOver pop = new PopOver;
        
    private FileInputStream fis;
            
        String Number, Name, Email, Contact, Id, Account,Route, Station;
        
        
   
   public static ObservableList<FarmersModel> FarmersList;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
   static  ResultSet result;
   static  ResultSet rs=null;
@FXML 
    private void clearFields(){
        txtfullname.clear();
        txtemail.clear();
        txtcontact.clear();
        txtid.clear();
        accountTF.clear();
        comboStage.getSelectionModel().clearSelection();
    }
    @FXML
    private void editFarmer(ActionEvent event) throws IOException {
        try{
               TreeItem<FarmersModel> farmer= (TreeItem<FarmersModel>)farmersTableView.getSelectionModel().getSelectedItem();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/com/gn/module/farmers/editFarmer.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        
        Stage stage = new Stage();
        //BoxBlur = new BoxBlur(3,3,3);
        stage.setScene(new Scene(p));
        //Creates a modal over the primary stage without adding a view on the task bar
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
        
        
         //Get controller of card 
            EditFarmerController card = loader.<EditFarmerController>getController();
            //Pass  data  
           card.transferData(farmer.getValue().getFarmer_number(), farmer.getValue().getFarmer_name(), farmer.getValue().getFarmer_email(), farmer.getValue().getFarmer_contact(),farmer.getValue().getFarmer_id() ,farmer.getValue().getFarmer_account() , farmer.getValue().getFarmer_station_id());
        
           stage.show();
        
    }catch(Exception e){
        e.printStackTrace();
        Alerts.info("Info", "Select the farmer details you want to edit");
    }
    }
    
    @FXML
    private void deleteFarmer(ActionEvent event)  {
     try{
       TreeItem<FarmersModel> farmer = (TreeItem<FarmersModel >) farmersTableView.getSelectionModel().getSelectedItem();
    
     }catch(NullPointerException v){
       Alerts.info("Info", "Select the farmer record to delete ");
     }
   }   
    @FXML
    private void suspendFarmer(ActionEvent event) throws IOException {
     Alerts.info("Information"," Working on this ");
   }   
    
 
    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
  
 
               addEffect(txtfullname);
               addEffect(txtemail);
               addEffect(txtcontact);
               addEffect(txtid);
               addEffect(accountTF);
               addEffect(comboStage);
               
          
               getStageName();
                comboStage.setItems(data);

               
               Mask.nameField(txtfullname);
               
               setupListeners();
               

        JFXTreeTableColumn<FarmersModel, String> FNumber= new JFXTreeTableColumn<>("Farmer Number");
               
             FNumber.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_number;
                       
                   }
         });
               
               JFXTreeTableColumn<FarmersModel, String> FName = new JFXTreeTableColumn<>(" Name");
               
               FName.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_name;
                       
                   }
               });
               
               JFXTreeTableColumn<FarmersModel, String> Email = new JFXTreeTableColumn<>(" Email");
               
               Email.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_email;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Contact = new JFXTreeTableColumn<>("Contact");
               
               Contact.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_contact;
                       
                   }
               });
               
               JFXTreeTableColumn<FarmersModel, String> Id = new JFXTreeTableColumn<>("Farmer Id");
               
               Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_id;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Account = new JFXTreeTableColumn<>("Account Number");
               
               Account.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_account;
                       
                   }
               });
               JFXTreeTableColumn<FarmersModel, String> Stage = new JFXTreeTableColumn<>("Station");
               
               Stage.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().farmer_station_id;
                       
                   }
               });
              JFXTreeTableColumn<FarmersModel, String> Status = new JFXTreeTableColumn<>("Status");
               
               Status.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<FarmersModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<FarmersModel, String> param) {
                       return param.getValue().getValue().status;
                       
                   }
               });
               
               FarmersList = FXCollections.observableArrayList();
               
               TreeItem<FarmersModel> root = new RecursiveTreeItem<FarmersModel>(FarmersList, RecursiveTreeObject::getChildren);
              
               farmersTableView.getColumns().addAll(FNumber, FName, Email, Contact, Id, Account, Stage, Status);
               farmersTableView.setRoot(root);
               farmersTableView.setShowRoot(false);
               
               addrowsToTable();
               
               
               searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                       farmersTableView.setPredicate(new Predicate<TreeItem<FarmersModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<FarmersModel> t) {
                               
                               boolean flag = t.getValue().farmer_name.getValue().contains(newValue)
                                       || t.getValue().farmer_email.getValue().contains(newValue)
                                       || t.getValue().farmer_contact.getValue().contains(newValue)
                                       || t.getValue().farmer_id.getValue().contains(newValue)
                                       || t.getValue().farmer_account.getValue().contains(newValue)
                                       || t.getValue().farmer_station_id.getValue().contains(newValue)
                                       || t.getValue().status.getValue().contains(newValue);
                               return flag;
                               
                           }
                       });
                   }
                           
               });

                
    }
 
       public void getStageName(){
            try {
                     String sql="SELECT * from milk_stock.milk_stations";
       
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         data.add(rs.getString("station_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }

     
  private void setupListeners(){
        txtfullname.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validFullName()){
                if(!newValue){
                    Flash swing = new Flash(box_fullname);
                    lbl_fullname.setVisible(true);
                    new SlideInLeft(lbl_fullname).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_fullname.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_fullname.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        txtemail.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_email);
                    lbl_email.setVisible(true);
                    new SlideInLeft(lbl_email).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_email.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_email.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(false);
            }
        });
        txtcontact.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_contact);
                    lbl_contact.setVisible(true);
                    new SlideInLeft(lbl_contact).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_contact.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_contact.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
  
        accountTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_account_number);
                    lbl_account.setVisible(true);
                    new SlideInLeft(lbl_account).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_account_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_account.setVisible(false);
                }
            }  else {
               lbl_error.setVisible(true);
            }
        });

        txtid.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validID()){
                if(!newValue){
                    Flash swing = new Flash(box_id);
                    lbl_id.setVisible(true);
                    new SlideInLeft(lbl_id).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_id.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_id.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        comboStage.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStation()){
                if(!newValue){
                    Flash swing = new Flash(box_stage);
                    lbl_stage.setVisible(true);
                    new SlideInLeft(lbl_stage).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_stage.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_stage.setVisible(false);
                }
            }  else {
             lbl_error.setVisible(true);
            }
        });
        }
        
        

            private boolean validFullName(){
                    return Mask.noSymbols(txtfullname) && !txtfullname.getText().isEmpty() && txtfullname.getLength() > 3  ;

                }
            private boolean validEmail(){
                    return Mask.isEmail(txtemail);
            }

            private boolean validContact(){
                    return Mask.noLetters(txtcontact) && Mask.noSpaces(txtcontact) && Mask.noSymbols(txtcontact) && !txtcontact.getText().isEmpty() && txtcontact.getLength() > 3 ;
                }
            private boolean validID(){
                    return Mask.noLetters(txtid) && Mask.noSpaces(txtid) && Mask.noSymbols(txtid) && !txtid.getText().isEmpty() && txtid.getLength() == 8 ;
                }
            private boolean validAccount(){
                    return Mask.noLetters(accountTF) && Mask.noSpaces(accountTF) && Mask.noInitSpace(accountTF) && Mask.noSymbols(accountTF) && !accountTF.getText().isEmpty() && accountTF.getLength() > 3 ;
                }
            private boolean validStation(){
                return !comboStage.getSelectionModel().isEmpty();
            }
            
       
@FXML
void addFarmer(ActionEvent event){
    if(validFullName() && validEmail() && validContact() && validID() && validAccount() && validStation() ){
        String status="Active";
        insert(txtfullname.getText(), txtemail.getText(), txtcontact.getText(), txtid.getText(), accountTF.getText(), comboStage.getValue(), status);
       FarmersList.add(new FarmersModel( txtnumber.getText(), txtfullname.getText(), txtemail.getText(), txtcontact.getText(), txtid.getText(), accountTF.getText(), comboStage.getValue(), status));
    } else if (!validFullName()){
            lbl_fullname.setVisible(true);
        } else if (!validEmail()) {
            lbl_email.setVisible(true);
        } else if(!validContact()){
            lbl_contact.setVisible(true);
        } else if(!validID()){
            lbl_id.setVisible(true);
        }else if(!validAccount()){
            lbl_account.setVisible(true);
        }else if(!validStation()){
            lbl_stage.setVisible(true);
        }
        else{
            lbl_error.setVisible(true);
        }
}
private void insert(String farmer_name, String farmer_email, String farmer_contact, String farmer_id, String farmer_account, String farmer_station_id, String status){
    
        sqlInsert= "INSERT INTO milk_stock.milk_farmers( farmer_name, farmer_email, farmer_contact, farmer_id, farmer_account_number, station_id, status) VALUES(?,?,?,?,?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, farmer_name);
            stat.setString(2, farmer_email);
            stat.setString(3, farmer_contact);
            stat.setString(4, farmer_id);
            stat.setString(5, farmer_account);
            stat.setString(6, farmer_station_id);
            stat.setString(7, status);
            stat.executeUpdate();
            
         
             Alerts.success("Success", farmer_name + " - added succesfully"); 
             
            refreshTable();
            
            EmailFarmer();

             
            clearFields();
   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
}

     static void addrowsToTable() {

        String sqlSelect = "SELECT  farmer_number, farmer_name, farmer_email, farmer_contact, farmer_id, farmer_account_number, station_id, status FROM milk_stock.milk_farmers";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                FarmersList.add(new FarmersModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5), result.getString(6), result.getString(7), result.getString(8)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }

    @FXML
    public void refreshTable (){
        refresh();
    }
    public static void refresh(){
          FarmersList.clear();
            addrowsToTable();
      }
      
      @FXML
      void messageFarmer(ActionEvent event){
        BasicAuth auth = new BasicAuth("bkgimgua", "pzuhxqzg");
            ApiHost host = new ApiHost(auth);
            // Instance of the Messaging API
            MessagingApi messagingApi = new MessagingApi(host);

            try {
                MessageResponse response = messagingApi.sendQuickMessage("+254790317338", "+254745344290", "MilkStock Test Sms ", "0790317338");

                Alerts.error("Server Error", "Server Response Status " + response.getStatus());
            } catch (HttpRequestException ex) {
                Alerts.error("Error", "Exception Server Response Status " + ex.getHttpResponse().getStatus());
                Alerts.error("Error", "Exception Server Response Body " + ex.getHttpResponse().getBodyAsString());
            }

        }
      

     void EmailFarmer(){
          try{
            String host ="smtp.gmail.com" ;
            String user = "milkstockdairycompany@gmail.com";
            String pass = "maziwamilk";
            String to = txtemail.getText();
            String from = "milkdairystockcompany@gmail.com";
            String subject = "Registration Successful.";
            String messageText = "Thanks for registering with us"
                    + "Your farmer id is 12345 :";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           
           System.out.println("message send successfully");
                Notifications  notification=Notifications.create()
                   // .graphic(new ImageView(image))
                    .title("Farmer Registration")
                    .text(" Email Sent")
                    .hideAfter(Duration.seconds(30))
                    .position(Pos.BOTTOM_RIGHT);                           
                    notification.showInformation();
        }catch(Exception ex){
            System.out.println(ex);
                Alerts.error("Error", ex.getMessage());   
    }

     }

     public void print(){
      try{
         TreeItem<FarmersModel> farmer = (TreeItem<FarmersModel >) farmersTableView.getSelectionModel().getSelectedItem();
                FXMLLoader loader= new FXMLLoader();
                     loader.setLocation(getClass().getResource("/com/gn/module/farmers/farmerCard.fxml"));
                   
                         loader.load();
            //Get controller of card 
            farmerCardController card = loader.<farmerCardController>getController();
            //Pass  data  
//          card.transferData(Number, Name,Email, Contact, Route, Station);
                   card.transferData(farmer.getValue().getFarmer_number(), farmer.getValue().getFarmer_name(), farmer.getValue().getFarmer_email(), farmer.getValue().getFarmer_contact(),farmer.getValue().getFarmer_id() ,farmer.getValue().getFarmer_account() , farmer.getValue().getFarmer_station_id());
        

            Parent root= loader.getRoot();
             Window window=null;
             PrinterJob job = PrinterJob.createPrinterJob();
             if(job != null){
                 job.showPrintDialog(window); 
                  job.printPage(root);
              job.endJob();
     }
      
     }catch(NullPointerException e){
         Alerts.info("Info", "Select the farmer details you want to print. ");
     }catch(IOException m){
         m.printStackTrace();
     }
     
     } 
    
     public void selectImage() {
         FileChooser imageFC= new FileChooser();
           //Set Extension filters
           FileChooser.ExtensionFilter extFilterJPG= new FileChooser.ExtensionFilter("JPG Files(*.JPG)", "*.JPG");
         FileChooser.ExtensionFilter extFilterjpg= new FileChooser.ExtensionFilter("jpg Files(*.jpg)", "*.jpg");
         FileChooser.ExtensionFilter extFilterPNG= new FileChooser.ExtensionFilter("PNG Files(*.PNG)", "*.PNG");
         FileChooser.ExtensionFilter extFilterpng= new FileChooser.ExtensionFilter("JPG Files(*.png)", "*.png");
         imageFC.getExtensionFilters().addAll(extFilterJPG, extFilterjpg,extFilterPNG, extFilterpng);
     
         File file= imageFC.showOpenDialog(null);
         try{
             BufferedImage bufferedImage = ImageIO.read(file);
             Image image=SwingFXUtils.toFXImage(bufferedImage, null);
             
             imageView.setImage(image);
         }catch(IOException ex){
             Alerts.info("Error", ex.getMessage());
         }
     }
                
}
