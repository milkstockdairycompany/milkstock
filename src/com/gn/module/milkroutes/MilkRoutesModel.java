
package com.gn.module.milkroutes;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MilkRoutesModel extends RecursiveTreeObject<MilkRoutesModel>{
    
  public StringProperty route_id, route_name, route_start_point,route_end_point;

    public MilkRoutesModel(String route_id, String route_name, String route_start_point, String route_end_point) {
        this.route_id = new SimpleStringProperty(route_id);
        this.route_name = new SimpleStringProperty(route_name);
        this.route_start_point = new SimpleStringProperty(route_start_point);
        this.route_end_point = new SimpleStringProperty(route_end_point);
    }

    public void setRoute_id(String route_id) {
        this.route_id =new SimpleStringProperty(route_id);
    }
    public void setRoute_name(String route_name) {
        this.route_name = new SimpleStringProperty(route_name);
    }
    public void setRoute_start_point(String route_start_point) {
        this.route_start_point = new SimpleStringProperty(route_start_point);
    }
    public void setRoute_end_point(String route_end_point) {
        this.route_end_point = new SimpleStringProperty(route_end_point);
    }

    public String getRoute_id() {
        return route_id.get();
    }
    
    public String getRoute_name() {
        return route_name.get();
    }
     public String getRoute_start_point() {
        return route_start_point.get();
    }
    public String getRoute_end_point() {
        return route_end_point.get();
    }

}
