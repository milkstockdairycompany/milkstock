
package com.gn.module.milkroutes;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MilkStationsModel extends RecursiveTreeObject<MilkStationsModel>{
    
  public StringProperty route_name, station_name, station_id;

    public MilkStationsModel(String route_name, String station_id, String station_name) {
        this.station_id = new SimpleStringProperty(station_id);
        this.route_name = new SimpleStringProperty(route_name);
        this.station_name = new SimpleStringProperty(station_name);
        
    }
    public void setStation_id(String station_id) {
        this.station_id = new SimpleStringProperty(station_id);
    }
    public void setRoute_name(String route_name) {
        this.route_name =new SimpleStringProperty(route_name);
    }
   
    public void setStation_name(String station_name) {
        this.station_name = new SimpleStringProperty(station_name);
    }


    public String getRoute_name() {
        return route_name.get();
    }
    
    public String getStation_id() {
        return station_name.get();
    }
    public String getStation_name() {
        return station_name.get();
    }

}
