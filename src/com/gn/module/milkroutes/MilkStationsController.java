package com.gn.module.milkroutes;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInDown;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javafx.util.Duration;

public class MilkStationsController implements Initializable{

    @FXML
    private JFXTextField StationNameTF;
    @FXML
    private ComboBox<String> comboBoxRoute;
    @FXML 
    private JFXTreeTableView<MilkStationsModel> Stations_Table;
    @FXML
    private HBox boxRoute, boxStation;
    @FXML
    private Label lbl_status, lbl_station_error, lbl_route_error;
    
    ObservableList<MilkStationsModel> StationList;
    
  String StationID, RouteID, StationName;
  
  private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    
    private void setupListeners(){
        comboBoxRoute.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRouteName()){
                if(!newValue){
                    Flash swing = new Flash(boxRoute);
                    //txtRouteName.validate();
                    //lbl_station_error.setText("Invalid name");
                    lbl_route_error.setVisible(true);
                    new SlideInDown(lbl_route_error).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxRoute.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_route_error.setVisible(false);
                }
            } else {
                lbl_status.setVisible(false);
            }
        });

        StationNameTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStationName()){
                if(!newValue){
                    Flash swing = new Flash(boxStation);
                    //txtRouteName.validate();
                    //lbl_station_error.setText("Invalid name");
                    lbl_station_error.setVisible(true);
                    new SlideInDown(lbl_station_error).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxStation.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_station_error.setVisible(false);
                }
            } else {
                lbl_status.setVisible(false);
            }
        });
    }   
        private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -info; -fx-border-color : -info");
        });
    

    }

      @Override
    public  void initialize(URL url, ResourceBundle rb) { 
          addEffect(comboBoxRoute);
          addEffect(StationNameTF);
          
          setupListeners();
          
      comboBoxRoute.getItems().addAll("1","2","3");
      
      JFXTreeTableColumn<MilkStationsModel, String> IDcolumn = new JFXTreeTableColumn<>("Station ID");
      
         IDcolumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkStationsModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkStationsModel, String> param) {
                return param.getValue().getValue().station_id;

            }
        });
          JFXTreeTableColumn<MilkStationsModel, String>RouteName = new JFXTreeTableColumn<>("Route Name");
                RouteName.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkStationsModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkStationsModel, String> param) {
                return param.getValue().getValue().route_name;

            }
        });   
        JFXTreeTableColumn<MilkStationsModel, String> StationName = new JFXTreeTableColumn<>("Station Name");
                StationName.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkStationsModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkStationsModel, String> param) {
                return param.getValue().getValue().station_name;

            }
        }); 
                
   StationList = FXCollections.observableArrayList();
        addrowsToTable();
        
        TreeItem<MilkStationsModel> root = new RecursiveTreeItem<MilkStationsModel>(StationList, RecursiveTreeObject::getChildren);
        Stations_Table.getColumns().addAll(IDcolumn, RouteName, StationName);
        Stations_Table.setRoot(root);
        Stations_Table.setShowRoot(false);
      

}
       void addrowsToTable() {

        String sqlSelect = "select * from milk_stock.milk_stations ";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                StationList.add(new MilkStationsModel(result.getString(1), result.getString(2), result.getString(3)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }  
    public void refreshTable(){
          StationList.clear();
          
        String sqlSelect = "select * from milk_stock.milk_stations ";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                StationList.add(new MilkStationsModel(result.getString(1), result.getString(2), result.getString(3) ));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       
      }
private boolean validStationName() {
     return Mask.noSymbols(StationNameTF) && Mask.noSymbols(StationNameTF) && !StationNameTF.getText().isEmpty() && StationNameTF.getLength() > 3;
 
    }

private boolean validRouteName() {
    
     return !comboBoxRoute.getSelectionModel().isEmpty();
     
     }
  
        @FXML
        private void addStation(ActionEvent event){
            try{
                if(validStationName() && validRouteName()){
                insert(StationNameTF.getText(), comboBoxRoute.getValue());
                }else if(!validRouteName()){
                    lbl_route_error.setVisible(true);
                }else if(!validStationName()){
                    lbl_station_error.setVisible(true);
                }
                else{
                    Alerts.error("Error","There is an error");
                }
            }
            catch (NumberFormatException c) {
                 Alerts.error("Error", c.getMessage());
            }
            catch (NullPointerException k) {
                 Alerts.error("Error", k.getMessage());
             } catch (Error e) {
                Alerts.error("Error", e.getMessage());
             } catch (Exception f) {
                Alerts.error("Error",f.getMessage());

        }
        }

    private void insert(String StationName, String RouteID){
            sqlInsert = "INSERT INTO milk_stock.milk_stations( route_id, station_name) VALUES (?,?)";
            
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, RouteID);
            stat.setString(2, StationName);
           

            stat.executeUpdate();
            
            clearFields();
             Alerts.success("Success", StationName + " - added succesfully"); 
          
             refreshTable();

        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
    }
    
    private void clearFields(){
        StationNameTF.clear();
        comboBoxRoute.getSelectionModel().clearSelection();
    }

}
