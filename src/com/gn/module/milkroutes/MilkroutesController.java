     
package com.gn.module.milkroutes;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import javafx.scene.control.Alert;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import com.jfoenix.validation.RequiredFieldValidator;
import com.lynden.gmapsfx.GoogleMapView;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.Duration;
import com.gn.module.milkroutes.MilkRoutesModel;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;






public class MilkroutesController implements Initializable {
    @FXML private JFXTextField txtRouteName, txtRouteStart, txtRouteEnd, searchTF,RouteIdTF, RouteNameTF, RouteStartTF, RouteEndTF;
    @FXML private HBox boxRouteName, boxStartingPoint,boxEndingPoint;
    @FXML private Label lblRouteName,lblStartPoint, lblEndPoint,  lbl_status;
    @FXML private JFXTreeTableView<MilkRoutesModel> Routes_Table;
    
     ObservableList<MilkRoutesModel> RouteList;
    
    String RouteId, RouteName, StartName, EndName;
      
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    
    
    private boolean validRouteName() {
     return  !txtRouteName.getText().isEmpty() && txtRouteName.getLength() > 3;
     //return Mask.nameField(txtRouteEnd);
    }
     private boolean validStartPoint() {
     return  !txtRouteStart.getText().isEmpty() && txtRouteStart.getLength() > 3 ; 
    }
    private boolean validEndPoint() {
     return  !txtRouteEnd.getText().isEmpty() && txtRouteEnd.getLength() > 3 ; 
    }
    
    @FXML
    void regist(ActionEvent event) {
      try{
        if( validRouteName() && validStartPoint() && validEndPoint() ){
         insert(txtRouteName.getText(), txtRouteStart.getText(), txtRouteEnd.getText()); 
         
         //RouteList.add( new MilkRoutesModel(txtRouteName.getText(),txtRouteStart.getText(),txtRouteEnd.getText()));
         
        } else if (!validRouteName()){
            lblRouteName.setVisible(true);
        } else if (!validStartPoint()) {
            lblStartPoint.setVisible(true);
        } else if(!validEndPoint()){
            boxEndingPoint.setStyle("-icon-color : -danger; -fx-border-color : -danger");
            lblEndPoint.setVisible(true);
        }
        else{
            Alerts.error("Error", "There is an error");
        }

          
     } catch (NumberFormatException c) {
         Alerts.error("Error", c.getMessage());
    }
    }
  private void insert(String name, String start_point, String end_point) {

        sqlInsert = "INSERT INTO milk_stock.milk_routes(route_name,route_start_point, route_end_point) VALUES (?,?,?)";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, name);
            stat.setString(2, start_point);
            stat.setString(3, end_point);

            stat.executeUpdate();
            
            clearFields();
             Alerts.success("Success", name + " - added succesfully"); 
          

        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
        refreshTable();
    }
    
    @FXML 
    private void clearFields(){
      clear();
    }
    

    private void setupListeners(){

        txtRouteName.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRouteName()){
                if(!newValue){
                    Flash swing = new Flash(boxRouteName);
                    //txtRouteName.validate();
                    lblRouteName.setText("Invalid name");
                    lblRouteName.setVisible(true);
                    new SlideInLeft(lblRouteName).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxRouteName.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lblRouteName.setVisible(false);
                }
            } else {
                lbl_status.setVisible(false);
            }
        });
        txtRouteStart.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStartPoint()){
                if(!newValue){
                    Flash swing = new Flash(boxRouteName);
                    //txtRouteStart.validate();
                    lblStartPoint.setText("Invalid Name");
                    lblStartPoint.setVisible(true);
                    new SlideInLeft(lblStartPoint).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxStartingPoint.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lblStartPoint.setVisible(false);
                }
            } else {
                lbl_status.setVisible(false);
            }
        });
        txtRouteEnd.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEndPoint()){
                if(!newValue){
                    Flash swing = new Flash(boxRouteName);
                    lblEndPoint.setVisible(true);
                    new SlideInLeft(lblEndPoint).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxEndingPoint.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lblEndPoint.setVisible(false);
                }
            } else {
                lbl_status.setVisible(false);
            }
        });
    }
    private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -info; -fx-border-color : -info");
        });
    

    }
    @Override
    public  void initialize(URL url, ResourceBundle rb) {
          addEffect(txtRouteName);
          addEffect(txtRouteStart);
          addEffect(txtRouteEnd);
          
          setupListeners();
          
    JFXTreeTableColumn<MilkRoutesModel, String> IDcolumn = new JFXTreeTableColumn<>("Route ID");

       IDcolumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkRoutesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkRoutesModel, String> param) {
                return param.getValue().getValue().route_id;

            }
        });
       
    JFXTreeTableColumn<MilkRoutesModel, String> Namecolumn = new JFXTreeTableColumn<>("Route Name");

       Namecolumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkRoutesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkRoutesModel, String> param) {
                return param.getValue().getValue().route_name;

            }
        });
       
    JFXTreeTableColumn<MilkRoutesModel, String> Startcolumn = new JFXTreeTableColumn<>("Start Point");

       Startcolumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkRoutesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkRoutesModel, String> param) {
                return param.getValue().getValue().route_start_point;

            }
        });
       
        JFXTreeTableColumn<MilkRoutesModel, String> Endcolumn = new JFXTreeTableColumn<>("End Point");

             Endcolumn.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkRoutesModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkRoutesModel, String> param) {
                return param.getValue().getValue().route_end_point;

            }
        });
             
     RouteList = FXCollections.observableArrayList();
        addrowsToTable();
        
        TreeItem<MilkRoutesModel> root = new RecursiveTreeItem<MilkRoutesModel>(RouteList, RecursiveTreeObject::getChildren);
        Routes_Table.getColumns().addAll(IDcolumn, Namecolumn, Startcolumn, Endcolumn);
        Routes_Table.setRoot(root);
        Routes_Table.setShowRoot(false);
        
    searchTF.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

               Routes_Table.setPredicate(new Predicate<TreeItem<MilkRoutesModel>>() {

                    @Override
                    public boolean test(TreeItem<MilkRoutesModel> t) {

                        boolean flag = t.getValue().route_id.getValue().contains(newValue)
                                || t.getValue().route_name.getValue().contains(newValue)
                                || t.getValue().route_start_point.getValue().contains(newValue)
                                || t.getValue().route_end_point.getValue().contains(newValue);
                              
                        return flag;

                    }
                });
            }

        });
    
        Routes_Table.getSelectionModel().selectedItemProperty().addListener((Observable, oldValue, newValue)
                -> 
                showDetails(newValue)
        );
    
    } 
        public void showDetails(TreeItem<MilkRoutesModel> pModel) {
            
        RouteIdTF.setText(pModel.getValue().getRoute_id());
        RouteNameTF.setText(pModel.getValue().getRoute_name());
        RouteStartTF.setText(pModel.getValue().getRoute_start_point());
        RouteEndTF.setText(pModel.getValue().getRoute_end_point());
        
        RouteId= pModel.getValue().getRoute_id();
        RouteName = pModel.getValue().getRoute_name();
        StartName = pModel.getValue().getRoute_start_point();
        EndName = pModel.getValue().getRoute_end_point();
       ;

    }

    
     void addrowsToTable() {

        String sqlSelect = "select * from milk_stock.milk_routes ";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                RouteList.add(new MilkRoutesModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4)));

            }
        } catch (SQLException r) {
            r.printStackTrace();
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }
         @FXML
    void deleteRouteRow(ActionEvent event) {
        try {
//            int index = Routes_Table.getSelectionModel().getSelectedIndex();
//            RouteList.remove(index);
//            String sqlSelect = " DELETE FROM milk_stock.milk_routes WHERE route_id='" + Routes_Table.getSelectionModel().getSelectedCells().get(index).getTreeItem().getValue().getRoute_id() + "'";
            String sqlSelect = " DELETE FROM milk_stock.milk_routes WHERE route_id='" + RouteIdTF.getText() + "'";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);
            stat.executeUpdate();
            clear();
            
        TrayNotification tray = new TrayNotification();
        tray.setNotificationType(NotificationType.SUCCESS);
        tray.setRectangleFill(Color.web("Red"));
        tray.setTitle("Success!");
        tray.setMessage("Record Deleted Successfully");
        tray.showAndDismiss(Duration.millis(10000));
        
        } catch (SQLException r) {
            r.printStackTrace();
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        }catch(ArrayIndexOutOfBoundsException e){
            Alerts.error("Error", e.getMessage());
            
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                rr.printStackTrace();
                Alerts.error("Error",rr.getMessage());
            }
        }
    }
        @FXML
    void updateRouteRow(ActionEvent event) {

        int index = Routes_Table.getSelectionModel().getSelectedIndex();
        TreeItem<MilkRoutesModel> pModel = Routes_Table.getSelectionModel().getSelectedItem();

        MilkRoutesModel MilkRoutesModel = new MilkRoutesModel(RouteIdTF.getText(), RouteNameTF.getText(), RouteStartTF.getText(), RouteEndTF.getText());
        pModel.setValue(MilkRoutesModel);

        String sqlUpdat = "UPDATE  milk_stock.milk_routes SET route_name='"+ RouteNameTF.getText() + "' ,route_start_point='" + RouteStartTF.getText() + "' , route_end_point='" + RouteEndTF.getText()+ "' WHERE route_name='" + RouteName + "' and" + " route_start_point='" + StartName + "'";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlUpdat);
            stat.executeUpdate();
            
            clear();
        TrayNotification tray = new TrayNotification();
        tray.setNotificationType(NotificationType.SUCCESS);
        tray.setRectangleFill(Color.web("Green"));
        tray.setTitle("Success!");
        tray.setMessage("Record Updated Successfully");
        tray.showAndDismiss(Duration.millis(10000));
        
        } catch (SQLException e) {
            Alerts.error("Error",e.getMessage());
            //Println(e.getMessage());
        } catch (ClassNotFoundException n) {

            Alerts.error("Error", n.getMessage());

        } catch (NumberFormatException f) {
            Alerts.error("Error",f.getMessage());

        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
      public void clear() {
          RouteIdTF.setText(null);
          RouteNameTF.setText(null);
          RouteStartTF.setText(null);
          RouteEndTF.setText(null);
      }
    
      
      public void refreshTable(){
          RouteList.clear();
          
        String sqlSelect = "SELECT * FROM milk_stock.milk_routes ";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                RouteList.add(new MilkRoutesModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       
      }
      @FXML
      public void refreshButton(ActionEvent event){
          refreshTable();
      }
        
      
}
