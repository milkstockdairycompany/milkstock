
package com.gn.module.staff;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;


public class EditStaffController implements Initializable {
    @FXML
    private Label lbl_error, lbl_staffname, lbl_staffemail, lbl_staffcontact, lbl_staffid, lbl_staffaccount, lbl_role;

    @FXML
    private HBox box_staffname, box_staffemail, box_staffcontact, box_staffid, box_account_number, box_role;  
    @FXML
    private JFXComboBox staffRoleCB;
    
    @FXML
    private JFXTextField staffNameTF, staffEmailTF, staffContactTF, staffIdTF, staffAccountTF, searchTF;

    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static final String url = "jdbc:mysql://localhost:3306";
    private static final String Password = "";
    private static final String username = "root";
    private static String sqlInsert;
    ResultSet result;
     String id;
     
    void transferData(  String ID, String Name, String Email, String Contact, String Id,String Account, String Role) {
      try{
          id =ID;
        staffNameTF.setText(Name);
        staffEmailTF.setText(Email);
        staffContactTF.setText(Contact);
        staffIdTF.setText(Id);
        staffAccountTF.setText(Account);
       staffRoleCB.setValue(Role);
      }catch(Exception e){
          Alerts.error("", "");
      }
    }
        
     private void setupListeners(){

        staffNameTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStaffName()){
                if(!newValue){
                    Flash swing = new Flash(box_staffname);
                    lbl_staffname.setVisible(true);
                    new SlideInLeft(lbl_staffname).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffname.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffname.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffEmailTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_staffemail);
                    lbl_staffemail.setVisible(true);
                    new SlideInLeft(lbl_staffemail).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffemail.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffemail.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffContactTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_staffcontact);
                    lbl_staffcontact.setVisible(true);
                    new SlideInLeft(lbl_staffcontact).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffcontact.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffcontact.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffIdTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validID()){
                if(!newValue){
                    Flash swing = new Flash(box_staffid);
                    lbl_staffid.setVisible(true);
                    new SlideInLeft(lbl_staffid).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffid.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffid.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffAccountTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_account_number);
                    lbl_staffaccount.setVisible(true);
                    new SlideInLeft(lbl_staffaccount).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_account_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffaccount.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffRoleCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRole()){
                if(!newValue){
                    Flash swing = new Flash(box_role);
                    lbl_role.setVisible(true);
                    new SlideInLeft(lbl_role).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_role.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_role.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });   
    }
            private boolean validStaffName(){
                    return Mask.noSymbols(staffNameTF) && !staffNameTF.getText().isEmpty()  ;

            }
            private boolean validEmail(){
                    return Mask.isEmail(staffEmailTF);
            }
            private boolean validContact(){
                    return Mask.noLetters(staffContactTF) && Mask.noSpaces(staffContactTF) && Mask.noSymbols(staffContactTF) && !staffContactTF.getText().isEmpty() && staffContactTF.getLength() > 3 ;
            }
            private boolean validID(){
                    return Mask.noLetters(staffIdTF) && Mask.noSpaces(staffIdTF) && Mask.noSymbols(staffIdTF) && !staffIdTF.getText().isEmpty() && staffIdTF.getLength() > 3 ;
            }
            private boolean validAccount(){
                    return Mask.noLetters(staffAccountTF) && Mask.noSpaces(staffAccountTF) && Mask.noInitSpace(staffAccountTF) && Mask.noSymbols(staffAccountTF) && !staffAccountTF.getText().isEmpty() && staffAccountTF.getLength() > 3 ;
            }
            private boolean validRole(){
                    return !staffRoleCB.getSelectionModel().isEmpty();
            }
      private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
     }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        staffRoleCB.getItems().addAll("Manager", "Record Keeper", "Driver", "Milk Collector");
        
                addEffect(staffNameTF);
                addEffect(staffEmailTF);
                addEffect(staffContactTF);
                addEffect(staffIdTF);
                addEffect(staffAccountTF);
                addEffect(staffRoleCB);
                
                setupListeners();
                
    }
    
    
     @FXML
    private void Close(ActionEvent event){
       Button btn = (Button) event.getSource();
      Stage stage =(Stage) btn.getScene().getWindow();
       stage.close();
       
    } 
    
    @FXML 
    private void UpdateStaff(ActionEvent event){
        if(validStaffName() && validEmail() && validContact() && validID() && validAccount() && validRole()){
        update();
              
      Button btn = (Button) event.getSource();
      Stage stage = (Stage) btn.getScene().getWindow();
      stage.close();  
      
    } else if (!validStaffName()){
            lbl_staffname.setVisible(true);
        } else if (!validEmail()) {
            lbl_staffemail.setVisible(true);
        } else if(!validContact()){
            lbl_staffcontact.setVisible(true);
        } else if(!validID()){
            lbl_staffid.setVisible(true);
        }else if(!validAccount()){
            lbl_staffaccount.setVisible(true);
        }else if(!validRole()){
            lbl_role.setVisible(true);
        }
        else{
            lbl_error.setVisible(true);
        }
        
        
       
   }
    
   public void update() {
       try{
             String sqlUpdate= "UPDATE milk_stock.milk_staff SET staff_name='"+staffNameTF.getText()+"', staff_email='"+staffEmailTF.getText()+"', staff_contact='"+staffContactTF.getText()+"', staff_id='"+staffNameTF.getText()+"', staff_account_number='"+staffAccountTF.getText()+"', staff_role= '"+staffRoleCB.getValue()+"' WHERE staff_number='"+id+"'" ; 
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlUpdate);

           
            stat.executeUpdate();
     
            
         
             Alerts.success("Success",staffNameTF.getText() + " - record updated succesfully"); 
     
            StaffController.refresh();
           
        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException | NumberFormatException | NullPointerException | Error ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());

        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
    } 
   }
}
