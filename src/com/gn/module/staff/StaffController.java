package com.gn.module.staff;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.gn.global.util.PasswordGenerator;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.function.Predicate;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;
import org.apache.commons.codec.digest.DigestUtils;

public class StaffController  implements Initializable{

    @FXML
    private JFXTreeTableView<StaffModel> staffTableView;
    
    @FXML
    private Label lbl_error, lbl_staffname, lbl_staffemail, lbl_username,  lbl_staffcontact, lbl_staffid, lbl_staffaccount, lbl_role;

    @FXML
    private HBox box_staffname, box_staffemail, box_username , box_staffcontact, box_staffid, box_account_number, box_role;  
    @FXML
    private JFXComboBox staffRoleCB;
    
    @FXML
    private JFXTextField staffNameTF, staffEmailTF, staffContactTF, staffIdTF, staffAccountTF, staffUsernameTF,searchTF;
    
  
    private String password,token;

    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static final String url = "jdbc:mysql://localhost:3306";
    private static final String Password = "";
    private static final String username = "root";
    private static String sqlInsert;
    static ResultSet result;
    
   static ObservableList<StaffModel> StaffList;
    
     private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
     }
    private void setupListeners(){

        staffNameTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validStaffName()){
                if(!newValue){
                    Flash swing = new Flash(box_staffname);
                    lbl_staffname.setVisible(true);
                    new SlideInLeft(lbl_staffname).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffname.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffname.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffEmailTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_staffemail);
                    lbl_staffemail.setVisible(true);
                    new SlideInLeft(lbl_staffemail).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffemail.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffemail.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffEmailTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_staffemail);
                    lbl_staffemail.setVisible(true);
                    new SlideInLeft(lbl_staffemail).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffemail.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffemail.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffUsernameTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_username);
                    lbl_username.setVisible(true);
                    new SlideInLeft(lbl_username).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_username.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_username.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffIdTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validContact()){
                if(!newValue){
                    Flash swing = new Flash(box_staffid);
                    lbl_staffid.setVisible(true);
                    new SlideInLeft(lbl_staffid).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_staffid.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffid.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffAccountTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_account_number);
                    lbl_staffaccount.setVisible(true);
                    new SlideInLeft(lbl_staffaccount).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_account_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_staffaccount.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        staffRoleCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAccount()){
                if(!newValue){
                    Flash swing = new Flash(box_role);
                    lbl_role.setVisible(true);
                    new SlideInLeft(lbl_role).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_role.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_role.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
        
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
                
                addEffect(staffNameTF);
                addEffect(staffEmailTF);
                addEffect(staffContactTF);
                addEffect(staffIdTF);
                addEffect(staffAccountTF);
                addEffect(staffRoleCB);
                addEffect(staffUsernameTF);
                setupListeners();
                staffRoleCB.getItems().addAll("Manager", "Record Keeper", "Driver", "Milk Collector");
                Mask.nameField(staffNameTF);
                
    JFXTreeTableColumn<StaffModel, String> Number = new JFXTreeTableColumn<>("Staff Number");

             Number.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_number;

            }
        });             
        JFXTreeTableColumn<StaffModel, String> Name = new JFXTreeTableColumn<>("Staff Name");

             Name.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_name;

            }
        }); 
        JFXTreeTableColumn<StaffModel, String> Email = new JFXTreeTableColumn<>("Staff Email");

             Email.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_email;

            }
        });
       JFXTreeTableColumn<StaffModel, String> User = new JFXTreeTableColumn<>("Staff Username");

             User.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_username;

            }
        });
        JFXTreeTableColumn<StaffModel, String> Contact = new JFXTreeTableColumn<>("Staff Contact");

             Contact.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_contact;

            }
        });
        JFXTreeTableColumn<StaffModel, String> Id = new JFXTreeTableColumn<>("Staff Id");

             Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_id;

            }
        }); 
        JFXTreeTableColumn<StaffModel, String> Account = new JFXTreeTableColumn<>("Staff Account");

             Account.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_account;

            }
        });
         JFXTreeTableColumn<StaffModel, String> Role = new JFXTreeTableColumn<>("Staff Role");

             Role.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<StaffModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<StaffModel, String> param) {
                return param.getValue().getValue().staff_role;

            }
        });          
             
        StaffList = FXCollections.observableArrayList(); 
        
        TreeItem<StaffModel> root = new RecursiveTreeItem<StaffModel>(StaffList, RecursiveTreeObject::getChildren);
        staffTableView.getColumns().addAll(Number,Name, Email, User , Contact, Id, Account, Role );
        staffTableView.setRoot(root);
        staffTableView.setShowRoot(false);
        
             addrowsToTable();
             
        searchTF.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
  
               staffTableView.setPredicate(new Predicate<TreeItem<StaffModel>>() {

                    @Override
                    public boolean test(TreeItem<StaffModel> t) {

                        boolean flag = t.getValue().staff_name.getValue().contains(newValue)
                                || t.getValue().staff_email.getValue().contains(newValue)
                                || t.getValue().staff_username.getValue().contains(newValue)
                                || t.getValue().staff_contact.getValue().contains(newValue)
                                || t.getValue().staff_id.getValue().contains(newValue)
                                || t.getValue().staff_account.getValue().contains(newValue);
                               
                        return flag;

                    }
                });
            }

        });  

    }

@FXML
void addStaff(ActionEvent event){
    if(validStaffName() && validEmail() &&validUsername() && validContact() && validID() && validAccount() && validRole() ){
         GeneratePassword();
         
        insert(staffNameTF.getText(), staffEmailTF.getText(),staffUsernameTF.getText(), password, token ,staffContactTF.getText(), staffIdTF.getText(), staffAccountTF.getText(), (String) staffRoleCB.getValue());
       //StaffList.add(new StaffModel(staffNameTF.getText(), staffEmailTF.getText(), staffContactTF.getText(), staffIdTF.getText(), staffAccountTF.getText() ));
    } else if (!validStaffName()){
            lbl_staffname.setVisible(true);
        } else if (!validEmail()) {
            lbl_staffemail.setVisible(true);
         
        } else if (!validUsername()) {
            lbl_username.setVisible(true);
        }else if(!validContact()){
            lbl_staffcontact.setVisible(true);
        } else if(!validID()){
            lbl_staffid.setVisible(true);
        }else if(!validAccount()){
            lbl_staffaccount.setVisible(true);
        }else if(!validRole()){
            lbl_role.setVisible(true);
        }
        else{
            lbl_error.setVisible(true);
        }
}

private void insert(String staff_name, String staff_email, String staff_username ,String  staff_pass, String token,String staff_contact, String staff_id, String account_number, String staff_role){
        sqlInsert= "INSERT INTO milk_stock.milk_staff(staff_name, staff_email, staff_username,staff_pass, staff_token ,staff_contact, staff_id, staff_account_number, staff_role) VALUES(?,?,?,?,?,?,?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, staff_name);
            stat.setString(2, staff_email);
            stat.setString(3, staff_username);
             stat.setString(4, staff_pass);
             stat.setString(5, token);
            stat.setString(6, staff_contact);
            stat.setString(7, staff_id);
            stat.setString(8, account_number);
            stat.setString(9, staff_role);
            stat.executeUpdate();
            
         
             Alerts.success("Success", staff_name + " - added succesfully"); 
             
              EmailStaffDetails();
              
            clearFields();
           
            
          
            refreshTable();
   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                Alerts.error("Error", ex.getMessage());
            }
        }
}
     void EmailStaffDetails(){
          try{
            String host ="smtp.gmail.com" ;
            String user = "milkstockdairycompany@gmail.com";
            String pass = "maziwamilk";
            String to = staffEmailTF.getText();
            String from = "milkdairystockcompany@gmail.com";
            String subject = "Login Details.";
            String messageText = "You have been registered with MilkStock Dairy. Your Login detail are your email" +to+" and your password is "+password+""
                    + ". Your reset password token is '"+token+" '";
                    
           
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           
           System.out.println("message sent successfully");
           

              TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(()-> {
                         //add notification in later
                      TrayNotification tray = new TrayNotification();
                        tray.setNotificationType(NotificationType.SUCCESS);
                        tray.setRectangleFill(Color.web("Green"));
                        tray.setTitle("Success!");
                        tray.setMessage("Login Details Sent To " +to );
                        tray.showAndDismiss(Duration.millis(10000));

                        }
                    );
                }
            };
             Timer timer = new Timer();
            timer.schedule(timerTask, 300);

            
        }catch(MessagingException ex){
            ex.printStackTrace();
                Alerts.error("Error in your network connection", "Connect to internet in order to send login credentials");   
    }

     }
  
  
@FXML 
    private void clearFields(){
        staffNameTF.clear();
        staffEmailTF.clear();
        staffContactTF.clear();
        staffIdTF.clear();
        staffAccountTF.clear();
        staffUsernameTF.clear();
    }
 

            private boolean validStaffName(){
                    return Mask.noSymbols(staffNameTF) && !staffNameTF.getText().isEmpty() && staffNameTF.getLength() > 3  ;

            }
            private boolean validEmail(){
                    return Mask.isEmail(staffEmailTF);
            }
            private boolean validUsername(){
                    return Mask.noSymbols(staffUsernameTF) && Mask.noSpaces(staffUsernameTF) && Mask.noInitSpace(staffUsernameTF) && !staffUsernameTF.getText().isEmpty() && staffUsernameTF.getLength() > 3 ;
            }
            private boolean validContact(){
                    return Mask.noLetters(staffContactTF) && Mask.noSpaces(staffContactTF) && Mask.noSymbols(staffContactTF) && !staffContactTF.getText().isEmpty() && staffContactTF.getLength() > 3 ;
            }
            private boolean validID(){
                    return Mask.noLetters(staffIdTF) && Mask.noSpaces(staffIdTF) && Mask.noSymbols(staffIdTF) && !staffIdTF.getText().isEmpty() && staffIdTF.getLength() > 3 ;
            }
            private boolean validAccount(){
                    return Mask.noLetters(staffAccountTF) && Mask.noSpaces(staffAccountTF) && Mask.noInitSpace(staffAccountTF) && Mask.noSymbols(staffAccountTF) && !staffAccountTF.getText().isEmpty() && staffAccountTF.getLength() > 3 ;
            }
            private boolean validRole(){
                    return !staffRoleCB.getSelectionModel().isEmpty();
            }
    static void addrowsToTable() {

        String sqlSelect = "SELECT staff_number, staff_name, staff_email, staff_username, staff_contact, staff_id, staff_account_number, staff_role FROM milk_stock.milk_staff";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
               StaffList.add(new StaffModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),  result.getString(6), result.getString(7), result.getString(8) ));

            }
        } catch (SQLException r) {
            r.printStackTrace();
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                rr.printStackTrace();
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }
    
    
    public static void refresh(){
        StaffList.clear();
        addrowsToTable();
        
    }
    
    @FXML
       public void refreshTable(){     
            refresh();   
      } 
     
       @FXML
       void deleteStaff(){
           try{
              TreeItem<StaffModel> staff = (TreeItem<StaffModel>)staffTableView.getSelectionModel().getSelectedItem(); 
                 String sqlSelect = " DELETE FROM milk_stock.milk_staff WHERE staff_number ='" + staff.getValue().getStaff_number() + "'";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);
            stat.executeUpdate();
            
            refresh();
            Alerts.info("Info", "  Staff deleted Successfully");
          
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
           }catch(NullPointerException t){   
            Alerts.info("Info", "Select the staff details to delete");
       }
           
     
    }
       
     @FXML
     void editStaff(ActionEvent event) throws IOException{
         
         try{
           TreeItem<StaffModel> staff = (TreeItem<StaffModel>)staffTableView.getSelectionModel().getSelectedItem();
                   
           FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/com/gn/module/staff/editStaff.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        
        Stage stage = new Stage();
        //BoxBlur = new BoxBlur(3,3,3);
        stage.setScene(new Scene(p));
        //Creates a modal over the primary stage without adding a view on the task bar
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
        
        
         //Get controller of card 
          EditStaffController card = loader.<EditStaffController>getController();
            //Pass  data  
           card.transferData( staff.getValue().getStaff_id(), staff.getValue().getStaff_name(), staff.getValue().getStaff_email(), staff.getValue().getStaff_contact() ,staff.getValue().getStaff_id(), staff.getValue().getStaff_account() , staff.getValue().getStaff_role());
        stage.show();
    }catch(NullPointerException e){
       Alerts.info("Info", "Select the staff details to edit"); 
    }catch(IOException i){
        i.printStackTrace();
    }
    }
     
    private void SuspendStaff(){
        Alerts.info("Ooops", "Working on this ");
    }
    
    
  
     private void GeneratePassword(){
        PasswordGenerator pass = new PasswordGenerator.PasswordGeneratorBuilder()
       
        .useLower(true)
        .useUpper(true)
        .build();
        password = pass.generate(8);
        
          PasswordGenerator ResetToken = new PasswordGenerator.PasswordGeneratorBuilder()
        .useDigits(true)
     
        .build();
        token = ResetToken.generate(4);
     }
     public void EmailTest(){
         
        final String username = "milkstockdairycompany@gmail.com";
        final String password = "maziwamilk";

        Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("milkstockdairycompany@gmail.com"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse("geekalex268@gmail.com")
            );
            message.setSubject("Testing Gmail TLS");
            message.setText("Dear Chibesa,"
                    + "\n\n Please do not spam my email!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
            
        }
    }
     }


