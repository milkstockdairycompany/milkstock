
package com.gn.module.staff;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class StaffModel extends RecursiveTreeObject<StaffModel> {

   
      public StringProperty  staff_number, staff_name, staff_email, staff_username ,staff_contact, staff_id, staff_account, staff_role;
  
      
      public StaffModel( String staff_number, String staff_name, String staff_email, String staff_username,String staff_contact, String staff_id,String staff_account , String staff_role){
          this.staff_number= new SimpleStringProperty(staff_number);
          this.staff_name = new SimpleStringProperty(staff_name);
          this.staff_email = new SimpleStringProperty(staff_email);
          this.staff_username= new SimpleStringProperty(staff_username);
          this.staff_contact = new SimpleStringProperty(staff_contact);
          this.staff_id =new SimpleStringProperty(staff_id);
          this.staff_account = new SimpleStringProperty(staff_account);
          this.staff_role = new SimpleStringProperty(staff_role);
         
      }
      
    public void set_Staffnumber(String staff_number) {
        this.staff_name = new SimpleStringProperty(staff_number);
    }
  
    public void set_Staffname(String staff_name) {
        this.staff_name = new SimpleStringProperty(staff_name);
    }
    public void setStaff_email(String staff_email) {
        this.staff_email = new SimpleStringProperty (staff_email);
    }
   public void setStaff_username(String staff_username) {
        this.staff_username = new SimpleStringProperty (staff_username);
    }
    public void setStaff_contact(String driver_contact) {
        this.staff_contact = new SimpleStringProperty (driver_contact);
    }
    public void setStaff_id(String staff_id) {
        this.staff_id = new SimpleStringProperty (staff_id);
    }
    public void setStaff_account(String staff_account) {
        this.staff_account = new SimpleStringProperty (staff_account);
    }
    public void setStaff_role(String staff_role) {
        this.staff_role = new SimpleStringProperty (staff_role);
    }


    public String getStaff_number() {
        return staff_number.get();
    }
    public String getStaff_name() {
        return staff_name.get();
    }
    public String getStaff_email() {
        return staff_email.get();
    }
    public String getStaff_username() {
        return staff_username.get();
    }
    public String getStaff_contact() {
        return staff_contact.get();
    }
    public String getStaff_id() {
        return staff_id.get();
    }

  public String getStaff_account() {
        return staff_account.get();
    }

    public String getStaff_role() {
        return staff_role.get();
    }  
    
}
