package com.gn.module.login;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.App;
import com.gn.GNAvatarView;
import com.gn.global.plugin.ViewManager;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.gn.global.util.PasswordGenerator;
import com.gn.module.main.Main;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.RotateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

public class recoverController implements Initializable{
     private static Connection conn=null;
        private static PreparedStatement stat=null;
        private static String url="jdbc:mysql://localhost:3306";
        private static String Password="" ;
        private static String sername="root";
        private static String sqlInsert;
        ResultSet result;
        
        String mail= null;
        String Pass= null;
        String Username= null;
        String id= null;
        
    @FXML
    private GNAvatarView avatar;

    @FXML
    private Label lbl_error;

    @FXML
    private HBox box_email, box_token;

    @FXML
    private TextField EmailTF;


    @FXML
    private TextField TokenTF;

    @FXML
    private Button login;

    @FXML
    private Label lbl_email, lbl_token;

     private RotateTransition rotateTransition = new RotateTransition();
  
     String password;

   

    @FXML
    void switchCreate(MouseEvent event) {
        App.decorator.setContent(ViewManager.getInstance().get("login"));
    }
   private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
                node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    }

    private void setupListeners(){
        EmailTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validEmail()){
                if(!newValue){
                    Flash swing = new Flash(box_email);
                    lbl_email.setVisible(true);
                    new SlideInLeft(lbl_email).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_email.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_email.setVisible(false);
                }
            } else {
                lbl_error.setVisible(false);
            }
        });

        TokenTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validToken()){
                if(!newValue){
                    Flash swing = new Flash(box_token);
                    lbl_token.setVisible(true);
                    new SlideInLeft(lbl_token).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_token.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_token.setVisible(false);
                }
            } else {
                lbl_error.setVisible(false);
            }
        });
    }

    private boolean validEmail(){
        return !EmailTF.getText().isEmpty() && Mask.isEmail(EmailTF) ;
    }

    private boolean validToken(){
        return !TokenTF.getText().isEmpty() && TokenTF.getLength() > 3 && Mask.noLetters(TokenTF);
    }
    @FXML
    private void recoverAction(ActionEvent event) {
        try {
             Pulse pulse = new Pulse(login);
             pulse.setDelay(Duration.millis(20));
             pulse.play();
             if(validEmail() && validToken()){
                 
               enter();  
             }
                 
             else  {
                 lbl_email.setVisible(true);
                 lbl_token.setVisible(true);
             }
        } catch (Exception ex) {
             ex.printStackTrace();
        
         }         
            
        
       
   }
     private void enter() {
         
        String sqlSelect="SELECT * FROM milk_stock.milk_staff WHERE staff_email='"+ EmailTF.getText().toString()+"' and staff_token='"+TokenTF.getText().toString()+"' ";
  
        try{
            
           Class.forName("com.mysql.jdbc.Driver");
           conn=DriverManager.getConnection(url,sername,Password);
           stat=conn.prepareStatement(sqlSelect);
       
        result=stat.executeQuery();
       
        
       if (result.next()){ 
           
           if ((result.getString("staff_email")).equals(EmailTF.getText()) && (result.getString("staff_token")).equals(TokenTF.getText()) ) {
               
              System.out.println(" Correct Details"); 
              
             id=result.getString("staff_number");
             mail = result.getString("staff_email"); 
             Pass = result.getString("staff_pass"); 
             Username = result.getString("staff_username"); 
             
             GeneratePass();
             
         
     }
       }
         else if (!result.next()) {
            lbl_error.setVisible(true);
        }
     }catch(ClassNotFoundException c){
         c.printStackTrace();
     }   catch (SQLException ex) {
           ex.printStackTrace();
         }
     }
  private void EmailStaffPass(){
          try{
            String host ="smtp.gmail.com" ;
            String user = "milkstockdairycompany@gmail.com";
            String pass = "maziwamilk";
            String to = mail ;
            String from = "milkdairystockcompany@gmail.com";
            String subject = " Recover Login Details.";
            String messageText = "We heard you lost your password . \n Your Login detail are your username: " +Username+" and your password is : "+password+" \n You can change your password in the Profile View ";
                    
           
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           
           System.out.println("Message sent successfully");
           
           Alerts.success("Success", "Login Credentials Sent to your Email");
            
            UpdatePass();
            
            App.decorator.setContent(ViewManager.getInstance().get("login"));
            
            
        }catch(MessagingException ex){
            ex.printStackTrace();
            
                Alerts.error("Error in your network connection", "Connect to internet in order to send login credentials");   
    }

     }
  
private void GeneratePass(){
     PasswordGenerator pass = new PasswordGenerator.PasswordGeneratorBuilder()
       
        .useLower(true)
        .useUpper(true)
        .build();
        Pass = pass.generate(8);
        
        EmailStaffPass();
}  

private void UpdatePass(){
    
             String sql ="UPDATE  milk_stock.milk_staff SET staff_pass= '"+Pass+"' WHERE staff_number='"+id+"' ";
           
        try {
            
           Class.forName("com.mysql.jdbc.Driver");
           conn=DriverManager.getConnection(url,sername,Password);
            stat = conn.prepareStatement(sql);
            stat.executeUpdate();

          System.out.println("Done");
           
        } catch (Exception e){
            e.printStackTrace();
        } 
    
}



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rotateTransition.setNode(avatar);
        rotateTransition.setByAngle(360);
        rotateTransition.setDuration(Duration.seconds(1));
        rotateTransition.setAutoReverse(true);
        
       
        addEffect(EmailTF);
        addEffect(TokenTF);
        
         setupListeners();
    }
}
     