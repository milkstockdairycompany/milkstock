
package com.gn.module.dashboard;

import com.gn.global.util.Alerts;
import eu.hansolo.tilesfx.Tile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.PieChart;


import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.scene.control.Label;

public class StaffDashboard implements Initializable {
    static Connection conn=com.gn.module.connection.Connect.Connect();
   static   PreparedStatement ps=null;
    static ResultSet rs=null;

  int countPie;
 
   @FXML private Tile calendar;
    @FXML private AreaChart<String, Number> areaChart;

    @FXML private PieChart pieChart;
 
       @FXML  private Label   paymentsLB, totalDeliveryLB, totalCreditsLB, totalRouteLB, totalStationLB;

    
    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        TotalDeliveries();
        TotalCredits();
        TotalRoutes();
        TotalStations();
        TotalPayments();
        
        
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                new PieChart.Data("Route 1", 20),
                new PieChart.Data("Route 2", 12),
                new PieChart.Data("Route 3", 25),
                new PieChart.Data("Route 4", 22),
                new PieChart.Data("Route 5", 30)
        );
        pieChart.setData(pieChartData);
        pieChart.setClockwise(false);

          
        //calendar.setDate();
    }


               public void TotalDeliveries(){
                    try {
                     String delivery="SELECT SUM(delivery_amount) FROM milk_deliveries ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalDeliveryLB.setText(rs.getString("SUM(delivery_amount)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalCredits(){
                    try {
                     String delivery="SELECT SUM(credit_amount) FROM milk_credits WHERE credit_status='approved' ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalCreditsLB.setText(rs.getString("SUM(credit_amount)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalRoutes(){
                    try {
                     String delivery="SELECT COUNT(route_id) FROM milk_routes ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalRouteLB.setText(rs.getString("COUNT(route_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalStations(){
                    try {
                     String delivery="SELECT COUNT(station_id) FROM milk_stations ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalStationLB.setText(rs.getString("COUNT(station_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
       public void TotalPayments(){
                    try {
                     String payments ="SELECT COUNT(payment_id) FROM milk_payments ";
                     ps=conn.prepareStatement(payments);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         paymentsLB.setText(rs.getString("COUNT(payment_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            
      public void Pie(String month) {
        countPie = 0;
        String sqlSelect = "select * from milk_deliveries where delivery_date like %";

        try {
            rs = ps.executeQuery();

            while (rs.next()) {
                countPie++;
            }

        } catch (SQLException r) {
            Alerts.error("Error",r.getMessage());
        } catch (NullPointerException l) {
          
        } finally {
            try {
                conn.close();
                ps.close();
            } catch (SQLException rr) {
                Alerts.error("Error", rr.getMessage());
            }
        }        
      }
}
