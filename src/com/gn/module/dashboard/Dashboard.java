
package com.gn.module.dashboard;

import com.gn.GNAvatarView;
import com.gn.global.util.Alerts;
import com.gn.lab.GNCalendarTile;

import eu.hansolo.tilesfx.Tile;
import java.awt.Desktop;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.net.URI;
import java.net.URISyntaxException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class Dashboard implements Initializable {
    
    Connection conn=com.gn.module.connection.Connect.Connect();
     PreparedStatement ps=null;
    ResultSet rs=null;

  int count;
 
   @FXML private GNCalendarTile calendar;
   
    @FXML private AreaChart<String, Number> areaChart;
 
    @FXML private PieChart pieChart;
 
    @FXML  private Label paymentsLB, totalVehiclesLB, totalDriversLB, totalFarmersLB, totalMilkMenLB, totalDeliveryLB, totalCreditsLB, totalRouteLB, totalStationLB;
    
    @FXML private GNAvatarView fb, twitter, gmail; 
    
    
    @SuppressWarnings("unchecked")
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        TotalFarmers();
        TotalVehicles();
        TotalDrivers();
        TotalMilkMen();
        TotalDeliveries();
        TotalCredits();
        TotalRoutes();
        TotalStations();
        TotalPayments();
        
       // calendar.setDate();
      
        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList(
                new PieChart.Data("Route 1", 20),
                new PieChart.Data("Route 2", 12),
                new PieChart.Data("Route 3", 25),
                new PieChart.Data("Route 4", 22),
                new PieChart.Data("Route 5", 30)
        );
        pieChart.setData(pieChartData);
        pieChart.setClockwise(false);

        
       XYChart.Series<String, Number> set = new XYChart.Series<>();
        
        chart(" '1%' ");
        set.getData().add(new XYChart.Data("January", count));
        
     
        chart(" '2%' ");
        set.getData().add(new XYChart.Data("February", count));

      
        chart(" '3%' ");
        set.getData().add(new XYChart.Data("March ", count));
        
  
        chart(" '4%' ");
        set.getData().add(new XYChart.Data("April", count));
        
     
        chart(" '5%' ");
        set.getData().add(new XYChart.Data("May", count));
        
     
        chart(" '6%' ");
        set.getData().add(new XYChart.Data("June", count));
        
   
        chart(" '7%' ");
        set.getData().add(new XYChart.Data("July ", count));
        
      
        chart(" '8%' ");
        set.getData().add(new XYChart.Data("August", count));
        
       
        chart(" '9%' ");
        set.getData().add(new XYChart.Data("September", count));
        
      
        chart(" '10%' ");
        set.getData().add(new XYChart.Data("October", count));
        
       
        chart(" '11%' ");
        set.getData().add(new XYChart.Data("November", count));
        
      
        chart(" '12%' ");
        set.getData().add(new XYChart.Data("December", count));

        areaChart.getData().setAll(set);
        areaChart.setCreateSymbols(true);
        
       fb.addEventHandler(MouseEvent.MOUSE_CLICKED,e ->{
        try {
        Desktop.getDesktop().browse(new URI("https://www.facebook.com"));

    } catch (IOException | URISyntaxException ex) {
        ex.printStackTrace();
        }
       });
       
       twitter.addEventHandler(MouseEvent.MOUSE_CLICKED,e ->{
        try {
        Desktop.getDesktop().browse(new URI("https://www.twitter.com"));

    } catch (IOException | URISyntaxException ex) {
        ex.printStackTrace();
        }
       });
       
       gmail.addEventHandler(MouseEvent.MOUSE_CLICKED,e ->{
        try {
        Desktop.getDesktop().browse(new URI("https://www.gmail.com"));

    } catch (IOException | URISyntaxException ex) {
        ex.printStackTrace();
        }
       });
    }

     //Return total of all farmers
                public void TotalFarmers(){
                    try {
                     String farmers="SELECT COUNT(farmer_number) FROM milk_farmers";
                     ps=conn.prepareStatement(farmers);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalFarmersLB.setText(rs.getString("COUNT(farmer_number)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
     //Return total of all milk vehicles
                public void TotalVehicles(){
                    try {
                     String vehicles="SELECT COUNT(vehicle_id) FROM milk_vehicles";
                     ps=conn.prepareStatement(vehicles);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalVehiclesLB.setText(rs.getString("COUNT(vehicle_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
     //Return total of all milk drivers
                public void TotalDrivers(){
                    try {
                     String drivers="SELECT COUNT(staff_number) FROM milk_staff WHERE staff_role ='driver' ";
                     ps=conn.prepareStatement(drivers);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalDriversLB.setText(rs.getString("COUNT(staff_number)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
     //Return total of all milk drivers
                public void TotalMilkMen(){
                    try {
                     String milkmen="SELECT COUNT(staff_number) FROM milk_staff WHERE staff_role ='Milk Collector' ";
                     ps=conn.prepareStatement(milkmen);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalMilkMenLB.setText(rs.getString("COUNT(staff_number)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
             public void TotalDeliveries(){
                    try {
                     String delivery="SELECT SUM(delivery_amount) FROM milk_deliveries ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalDeliveryLB.setText(rs.getString("SUM(delivery_amount)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalCredits(){
                    try {
                     String delivery="SELECT SUM(credit_amount) FROM milk_credits WHERE credit_status='approved' ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalCreditsLB.setText(rs.getString("SUM(credit_amount)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalRoutes(){
                    try {
                     String delivery="SELECT COUNT(route_id) FROM milk_routes ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalRouteLB.setText(rs.getString("COUNT(route_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalStations(){
                    try {
                     String delivery="SELECT COUNT(station_id) FROM milk_stations ";
                     ps=conn.prepareStatement(delivery);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         totalStationLB.setText(rs.getString("COUNT(station_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
            public void TotalPayments(){
                    try {
                     String payments ="SELECT COUNT(payment_id) FROM milk_payments ";
                     ps=conn.prepareStatement(payments);
                     rs=ps.executeQuery();
                     while(rs.next()){
                         paymentsLB.setText(rs.getString("COUNT(payment_id)"));

                     }
                 } catch (SQLException ex) {
                    Alerts.error("Error", ex.getMessage());
                 }
                
            }
 
      public void chart(String month) {
        count = 0;
        String s="";
        String areachart = "SELECT SUM(delivery_amount) FROM milk_deliveries WHERE delivery_date LIKE " + month;

        try {
             ps=conn.prepareStatement(areachart);
            rs = ps.executeQuery();

            while (rs.next()) {
                 s = rs.getString("SUM(delivery_amount)");
                if(s !=null){
                count = parseInt( s); 
               }else{
                  count=0; 
               }
            }

        } catch (SQLException r) {
            Alerts.error("Error",r.getMessage());
            r.printStackTrace();
        } catch (NullPointerException l) {
          l.printStackTrace();
        }         
      }
      
 
}