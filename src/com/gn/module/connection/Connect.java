
package com.gn.module.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import com.gn.global.util.Alerts;
import java.sql.SQLException;


public class Connect {
    Connection conn = null;
    public static Connection Connect()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/milk_stock", "root", "");
            System.out.println("Connection Successful");
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
            Alerts.error("", ex.getMessage());
           return null;
        }
    }
}



