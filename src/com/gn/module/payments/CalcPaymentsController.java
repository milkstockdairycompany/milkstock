
package com.gn.module.payments;

import animatefx.animation.Flash;
import animatefx.animation.SlideInDown;
import static com.gn.global.util.AddEffect.addEffect;
import com.gn.global.util.Alerts;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import static java.lang.Integer.parseInt;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Duration;

public class CalcPaymentsController implements Initializable{
    
     @FXML
     private Label rateLB;
    
    @FXML
    private JFXDatePicker payFromDP,  payToDP;
        @FXML
    private VBox boxFrom , boxTo;


    @FXML
    private Label payFromLB, payToLB, lb_error  ;

    @FXML
    private JFXTreeTableView<PaymentsModel> PaymentsTable;
    
     ObservableList<PaymentsModel> PayList;
     ObservableList<PaymentsModel> PaymentsList;
          
   Connection conn=com.gn.module.connection.Connect.Connect();
    
    ResultSet rs=null;
    private static PreparedStatement payPS = null;
     private static PreparedStatement creditPS = null;
     private static PreparedStatement updatePS= null;
     private static PreparedStatement deliveryPS= null;
     
    private static final String url = "jdbc:mysql://localhost:3306/milk_stock";
    private static final String Password = "";
    private static final String username = "root";
   
    
    private static String fno, fname, total, netpay, to, p, from;
    private static Integer c, rate;
    ResultSet payRS;
    ResultSet creditRS;
    ResultSet updateRS;
    
    
  public void CalculatePayments(){

  String sql ="SELECT milk_farmers.farmer_number, milk_farmers.farmer_name, milk_farmers.farmer_account_number,SUM(milk_deliveries.delivery_amount) AS total, SUM(milk_credits.credit_amount) AS credits FROM milk_deliveries LEFT OUTER JOIN milk_farmers ON milk_deliveries.farmer_number=milk_farmers.farmer_number LEFT OUTER JOIN milk_credits ON milk_credits.farmer_number = milk_deliveries.farmer_number WHERE delivery_status='Good' AND delivery_date BETWEEN '"+payFromDP.getEditor().getText()+"' AND '"+payToDP.getEditor().getText()+"' GROUP by milk_farmers.farmer_number";
    
  String pay =" SELECT rateSh FROM milk_stock.milk_payrates WHERE payrateFrom = '"+payFromDP.getEditor().getText()+"' AND payrateTo ='"+payToDP.getEditor().getText()+ "' ";


  
  System.out.println(payToDP.getEditor().getText());    
  try {

       to =payFromDP.getEditor().getText();
       from =payToDP.getEditor().getText();
      
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            payPS = conn.prepareStatement(sql);
            
            creditPS=conn.prepareStatement(pay);
            
           
            
            
            this.payRS = payPS.executeQuery();
            
            creditRS=creditPS.executeQuery();
            
             
              
            while (this.payRS.next() && creditRS.first() ) {
               
                
                 fno= this.payRS.getString("farmer_number");
                 fname= this.payRS.getString("farmer_name");
                String faccount= this.payRS.getString("farmer_account_number");
                 total = this.payRS.getString("total");
                String credits = this.payRS.getString("credits");
                
              String r= creditRS.getString("rateSh");
                
              
               rateLB.setText(creditRS.getString("rateSh"));
                
                Integer t=parseInt(total.trim());
                  if(credits !=null){
                c = parseInt( credits ); 
               }else{
                  c=0; 
               } 
               
                 rate= parseInt(r.trim());
                
                
                Integer totalpay= t*rate ;
                Integer net= totalpay - c;
                
                String cm= Integer.toString(c); 
                 p=Integer.toString(totalpay); 
                netpay=Integer.toString(net);
                
              PayList.add(new PaymentsModel(fno, fname, faccount, total, cm , p, netpay));
              
            //  GeneratePayment(fno, total, p, c, netpay, from,to );
              
                   
           
         }
        } catch (SQLException r) {
            r.printStackTrace();
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
             System.out.println(n);
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
             l.printStackTrace();
            Alerts.error(" Null Error",l.getMessage());
        } finally {
            try {
                conn.close();
                payPS.close();
            } catch (SQLException rr) {
                 rr.printStackTrace();
                Alerts.error("Error",rr.getMessage());
            }
        }
      
  
 
  
  
    
}
  
  @FXML
  public void pushPayments(){
      GeneratePayment(fno, total, rate ,p, c, netpay, from,to );
  }
        private boolean validPayFrom(){
                   return  !payFromDP.getEditor().getText().isEmpty() && payFromDP.getValue().isBefore(LocalDate.now());
            }
        private boolean validPayTo(){
             return  !payToDP.getEditor().getText().isEmpty() && payToDP.getValue().isBefore(LocalDate.now()) && payToDP.getValue().isAfter(payFromDP.getValue());
                  // return   payToDP.getValue().isBefore(LocalDate.now()) && payToDP.getValue().isAfter(payFromDP.getValue()) && !payToDP.getEditor().getText().isEmpty();
            }
      
  public void GeneratePayment(String number, String total_delivery, Integer rate, String pay, Integer ded, String net, String from, String to){
      
      
      String insert="INSERT INTO milk_payments (farmer_number,total_delivery, rate, total_pay, total_deduction,net_pay, pay_from, pay_to ) VALUES (?,?,?,?,?,?,?, ?)";
        
      String update =" UPDATE milk_stock.milk_deliveries SET delivery_status =' Paid ' WHERE delivery_date BETWEEN '"+payFromDP.getEditor().getText()+"' AND '"+payToDP.getEditor().getText()+"' ";

    //  String credits =" UPDATE milk_stock.milk_credits SET delivery_status =' Paid ' WHERE delivery_date BETWEEN '"+payFromDP.getEditor().getText()+"' AND '"+payToDP.getEditor().getText()+"' ";
    
      
         try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            
            updatePS = conn.prepareStatement(insert);
                    
            updatePS.setString(1, number);
            updatePS.setString(2, total_delivery);
            updatePS.setInt(3, rate);
            updatePS.setString(4, pay);
            updatePS.setInt(5, ded);
            updatePS.setString(6, net);
            updatePS.setString(7, from);
            updatePS.setString(8, to);
           
            updatePS.executeUpdate(); 
            
            
            deliveryPS = conn.prepareStatement(update);
            deliveryPS.executeUpdate();
            
            PayList.clear();
            
            Alerts.info("Information", "Payment Calculated Successful");
  }catch(Exception e){
      e.printStackTrace();
  }
  }
  
  
  
  
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        addEffect(payFromDP);
        addEffect(payToDP);
        
        setupListeners();
        
            JFXTreeTableColumn<PaymentsModel, String> Number= new JFXTreeTableColumn<>("Farmer Number");
  
             Number.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().number;
                       
                   }
               });
            JFXTreeTableColumn<PaymentsModel, String> Name= new JFXTreeTableColumn<>("Farmer Name");
  
             Name.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().name;
                       
                   }
               });
            JFXTreeTableColumn<PaymentsModel, String> Account= new JFXTreeTableColumn<>("Farmer Account");
  
             Account.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().account;
                       
                   }
               });
            JFXTreeTableColumn<PaymentsModel, String> Deliveries = new JFXTreeTableColumn<>("Total Deliveries");
  
             Deliveries.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().totaldeliveries;
                       
                   }
               });
            JFXTreeTableColumn<PaymentsModel, String> Credits= new JFXTreeTableColumn<>("Total Credits");
  
             Credits.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().credits;
                       
                   }
               });

            JFXTreeTableColumn<PaymentsModel, String> Pay= new JFXTreeTableColumn<>("Total Pay");
  
             Pay.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().totalpay;
                       
                   }
               });
            JFXTreeTableColumn<PaymentsModel, String> Net= new JFXTreeTableColumn<>("Net Pay");
  
             Net.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentsModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentsModel, String> param) {
                       return param.getValue().getValue().netpay;
                       
                   }
               });
             
               PayList = FXCollections.observableArrayList();
               
               TreeItem<PaymentsModel> root = new RecursiveTreeItem<>(PayList, RecursiveTreeObject::getChildren);
               PaymentsTable.getColumns().addAll(Number, Name, Account, Deliveries,Pay, Credits, Net);
               PaymentsTable.setRoot(root);
               PaymentsTable.setShowRoot(false);
               
              
    }
    private void setupListeners(){
        payFromDP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validPayFrom()){
                if(!newValue){
                    Flash swing = new Flash(boxFrom);
                    payFromLB.setVisible(true);
                    new SlideInDown(payFromLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxFrom.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    payFromLB.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            }); 
        payToDP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validPayTo()){
                if(!newValue){
                    Flash swing = new Flash(boxTo);
                    payToLB.setVisible(true);
                    new SlideInDown(payToLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxTo.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    payToLB.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
}
}
