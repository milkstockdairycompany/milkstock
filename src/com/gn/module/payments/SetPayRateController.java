package com.gn.module.payments;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

public class SetPayRateController implements Initializable{
  
    @FXML
    private Label lb_error;

    @FXML
    private HBox monthBox, yearBox, rateBox;

    @FXML
    private JFXDatePicker YearDP, MonthDP;

    @FXML
    private JFXTextField rateTF;
    
    @FXML
    private Label lb_month,  lb_year, lb_rate;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs=null;
   
  
    private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    

    }
     private void setupListeners(){
        MonthDP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validPayFrom()){
                if(!newValue){
                    Flash swing = new Flash(monthBox);
                    lb_month.setVisible(true);
                    new SlideInLeft(lb_month).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                   monthBox.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_month.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
        YearDP.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validPayTo()){
                if(!newValue){
                    Flash swing = new Flash(yearBox);
                    lb_year.setVisible(true);
                    new SlideInLeft(lb_year).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                   yearBox.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_year.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
        rateTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validRate()){
                if(!newValue){
                    Flash swing = new Flash(rateBox);
                    lb_rate.setVisible(true);
                    new SlideInLeft(lb_rate).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                   rateBox.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_rate.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
     }
@FXML
   private void AddPayRate(){
     if(validPayFrom() && validPayTo() && validRate()){
         insert(MonthDP.getValue().toString(), YearDP.getValue().toString(), rateTF.getText() );
     }else if(!validPayFrom()){
       lb_month.setVisible(true);
       
     }else if(!validPayTo()){
         lb_year.setVisible(true);
     } else if (!validRate()){
         lb_rate.setVisible(true);
     }
     else{
            lb_error.setVisible(true);
        }
    }
   private void insert(String payrateFrom, String payrateTo, String rateSh ){
        sqlInsert= "INSERT INTO milk_stock.milk_payrates( payrateFrom, payrateTo, rateSh ) VALUES(?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, payrateFrom);
            stat.setString(2, payrateTo);
            stat.setString(3, rateSh);

            
            stat.executeUpdate();
            
         
             Alerts.success("Success",   "Rate added succesfully"); 
             
             ClearFields();
             
         

   
        } catch (SQLException ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException | NumberFormatException | NullPointerException | Error ex) {
            ex.printStackTrace();
            Alerts.error("Error", ex.getMessage());

        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                Alerts.error("Error", ex.getMessage());
            }
        }
}
  @FXML  
  private void ClearFields(){
      rateTF.clear();  
    }
    
    private boolean validPayFrom(){
        return !MonthDP.getEditor().getText().isEmpty();
    }
    private boolean validPayTo(){
        return !YearDP.getEditor().getText().isEmpty();
    }
    private boolean validRate(){
        return  Mask.noInitSpace(rateTF) && Mask.noSpaces(rateTF) && Mask.noLetters(rateTF) && Mask.noSymbols(rateTF) && !rateTF.getText().isEmpty() ;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        addEffect(MonthDP);   
        addEffect(YearDP);
        addEffect(rateTF);
        
        setupListeners(); 
          

             
    }

   

}
