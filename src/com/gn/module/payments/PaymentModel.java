package com.gn.module.payments;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class PaymentModel  extends RecursiveTreeObject<PaymentModel> {
   
     public   StringProperty pay_id, farmer_number, farmer_name, farmer_account, farmer_phone, totaldeliveries, rate ,totalpay, credits,netpay, pay_from, pay_to, status;
     
  public PaymentModel(String pay_id, String farmer_number, String farmer_name, String farmer_account, String farmer_phone, String totaldeliveries,String rate , String totalpay,String credits , String netpay, String pay_from, String pay_to, String status) {
    this.pay_id = new SimpleStringProperty(pay_id);   
    this.farmer_number = new SimpleStringProperty(farmer_number);
    this.farmer_name = new SimpleStringProperty(farmer_name);
    this.farmer_account = new SimpleStringProperty(farmer_account);
    this.farmer_phone = new SimpleStringProperty(farmer_phone);
    this.totaldeliveries = new SimpleStringProperty(totaldeliveries);
    this.rate = new SimpleStringProperty(rate);
    this.totalpay = new SimpleStringProperty(totalpay);
    this.credits = new SimpleStringProperty(credits);
    this.netpay =new SimpleStringProperty(netpay);
    this.pay_from =new SimpleStringProperty(pay_from);
    this.pay_to =new SimpleStringProperty(pay_to);
    this.status =new SimpleStringProperty(status);
}

    public StringProperty getPay_id() {
        return pay_id;
    }

    public void setPay_id(StringProperty pay_id) {
        this.pay_id = pay_id;
    }

    public StringProperty getFarmer_number() {
        return farmer_number;
    }

    public void setFarmer_number(StringProperty farmer_number) {
        this.farmer_number = farmer_number;
    }

    public StringProperty getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(StringProperty farmer_name) {
        this.farmer_name = farmer_name;
    }

    public StringProperty getFarmer_account() {
        return farmer_account;
    }

    public void setFarmer_account(StringProperty farmer_account) {
        this.farmer_account = farmer_account;
    }

    public StringProperty getFarmer_phone() {
        return farmer_phone;
    }

    public void setFarmer_phone(StringProperty farmer_phone) {
        this.farmer_phone = farmer_phone;
    }

    public StringProperty getTotaldeliveries() {
        return totaldeliveries;
    }

    public void setTotaldeliveries(StringProperty totaldeliveries) {
        this.totaldeliveries = totaldeliveries;
    }

    public StringProperty getRate() {
        return rate;
    }

    public void setRate(StringProperty rate) {
        this.rate = rate;
    }

    public StringProperty getTotalpay() {
        return totalpay;
    }

    public void setTotalpay(StringProperty totalpay) {
        this.totalpay = totalpay;
    }

    public StringProperty getCredits() {
        return credits;
    }

    public void setCredits(StringProperty credits) {
        this.credits = credits;
    }

    public StringProperty getNetpay() {
        return netpay;
    }

    public void setNetpay(StringProperty netpay) {
        this.netpay = netpay;
    }

    public StringProperty getPay_from() {
        return pay_from;
    }

    public void setPay_from(StringProperty pay_from) {
        this.pay_from = pay_from;
    }

    public StringProperty getPay_to() {
        return pay_to;
    }

    public void setPay_to(StringProperty pay_to) {
        this.pay_to = pay_to;
    }

    public StringProperty getStatus() {
        return status;
    }

    public void setStatus(StringProperty status) {
        this.status = status;
    }

 


  
  
  
  
}
