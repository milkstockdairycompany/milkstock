
package com.gn.module.payments;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Alex
 */
public class PaymentsModel extends RecursiveTreeObject<PaymentsModel> {
    

    public   StringProperty number, name, account, totaldeliveries, credits ,totalpay, netpay;
   
    public PaymentsModel(String number, String name, String account, String deliveries, String credits, String totalpay, String netpay ){
        this.number = new SimpleStringProperty(number);
        this.name = new SimpleStringProperty(name);
        this.account= new SimpleStringProperty(account);
        this.totaldeliveries = new SimpleStringProperty(deliveries);
        this.credits = new SimpleStringProperty(credits);
        this.totalpay = new SimpleStringProperty(totalpay);
        this.netpay = new SimpleStringProperty(netpay);
     
    
}



    public StringProperty getNumber() {
        return number;
    }

    public void setNumber(StringProperty number) {
        this.number = number;
    }

    public StringProperty getName() {
        return name;
    }

    public void setName(StringProperty name) {
        this.name = name;
    }

    public StringProperty getAccount() {
        return account;
    }

    public void setAccount(StringProperty account) {
        this.account = account;
    }

    public StringProperty getTotaldeliveries() {
        return totaldeliveries;
    }

    public void setTotaldeliveries(StringProperty totaldeliveries) {
        this.totaldeliveries = totaldeliveries;
    }

    public StringProperty getCredits() {
        return credits;
    }

    public void setCredits(StringProperty credits) {
        this.credits = credits;
    }

    public StringProperty getTotalpay() {
        return totalpay;
    }

    public void setTotalpay(StringProperty totalpay) {
        this.totalpay = totalpay;
    }

    public StringProperty getNetpay() {
        return netpay;
    }

    public void setNetpay(StringProperty netpay) {
        this.netpay = netpay;
    }

   
    

   
    }
    
     

