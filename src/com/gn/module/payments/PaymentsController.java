package com.gn.module.payments;

import com.gn.global.util.Alerts;
import com.gn.module.mpesa.Constants;
import com.gn.module.mpesa.Mpesa;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.util.Callback;

public class PaymentsController implements Initializable{

    @FXML
    private JFXTextField searchTF;

    @FXML
    private JFXTreeTableView<PaymentModel> paymentTableView;
    
   private static ObservableList<PaymentModel> PaymentsList;
   
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306/milk_stock";
    private static String Password = "";
    private static String username = "root";
   
   
   static  ResultSet rs=null;
   
        static void addrowsToTable() {

        String sqlSelect = "SELECT milk_payments.payment_id, milk_farmers.farmer_number, milk_farmers.farmer_name, milk_farmers.farmer_account_number, milk_farmers.farmer_contact, milk_payments.total_delivery, milk_payments.rate, milk_payments.total_pay, milk_payments.total_deduction, milk_payments.net_pay, milk_payments.pay_from, milk_payments.pay_to , milk_payments.status FROM milk_payments, milk_farmers WHERE milk_farmers.farmer_number =milk_payments.farmer_number";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            rs = stat.executeQuery();

            while (rs.next()) {
               PaymentsList.add(new PaymentModel(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13)));

            }
        } catch (SQLException r) {
            r.printStackTrace();
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            n.printStackTrace();
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            l.printStackTrace();
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                rr.printStackTrace();
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }

    @FXML
    public void refreshTable (){
        refresh();
    }
    public static void refresh(){
          PaymentsList.clear();
            addrowsToTable();
      }
     @FXML
    private void pay(){
          try{
           TreeItem<PaymentModel> pay = (TreeItem<PaymentModel>)paymentTableView.getSelectionModel().getSelectedItem();
           
           mpesaPay(pay.getValue().getFarmer_phone(), pay.getValue().getNetpay() );
           
    } catch(NullPointerException e){
      
       Alerts.info("Info", "Select the farmer payment to make");
    }
    }
         @FXML
    private void paySlips(){
          try{
           TreeItem<PaymentModel> pay = (TreeItem<PaymentModel>)paymentTableView.getSelectionModel().getSelectedItem();
           
           mpesaPay(pay.getValue().getFarmer_phone(), pay.getValue().getNetpay() );
           
    } catch(NullPointerException e){
      
       Alerts.info("Info", "Select the farmer payment to print slip");
    }
    }
   
    private void mpesaPay(StringProperty mpesaNo, StringProperty pay){
       System.out.println(mpesaNo);
        System.out.println(pay);
        
        try{
        Mpesa m=new Mpesa(Constants.APP_KEY,Constants.APP_SECRET);
        m.authenticate();
        
        m.B2CRequest("apiop5","gfVt3rSPv3z39xbOHgClEZkJJ0gUTQmZ2ryEjwbXbUHxmFpqn1RR0x25TOSC/1VFUDnQlnxo8pdvsrQ8WdawBKoJCNUANa2D9+DGBfV8jvphBYZH7D6KDvRLFi4E2NlskV6FyU5+Ju42Hb33KeRochIPAYJKACpEpHdoN8E1wmvLef7Xr+vQZ/r8sBBvSS6hj5cGGNkzvBtuZWNp5EseLdr1PMmWlotCMQ3iCVj7Q/dHt/OxTuF+u/PpehK+sPLa8I2le40iCNgJimvTkUvKHf+GDB2we4bpc+nuYkL5IN5Re7vA8hnGRzhW17xAlqgUYI89RIzw6z6vIKgX4MZjkA==","BusinessPayment", "'"+pay+"'" ,"600576", "'254"+mpesaNo+"'" ,"This","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BConfirmation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation","http://obscure-bayou-52273.herokuapp.com/api/Mpesa/C2BValidation");

         Alerts.success("Success", "Mpesa Payment Successful");
    }catch(IOException e){
        e.printStackTrace();
        Alerts.warning("Warning ", "Check your internet connection");
    }catch(Exception c){
        c.printStackTrace();
        Alerts.error("Error In Payment", c.getMessage());
    }
    }
    
   
    @Override
    public void initialize(URL location, ResourceBundle resources) {
         JFXTreeTableColumn<PaymentModel, String> payID= new JFXTreeTableColumn<>("Payment Id");
  
             payID.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().pay_id;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> No= new JFXTreeTableColumn<>("Farmer Number");
  
             No.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().farmer_number;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Name= new JFXTreeTableColumn<>("Farmer Name");
  
             Name.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().farmer_name;
         
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Account= new JFXTreeTableColumn<>("Account Number");
  
             Account.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().farmer_account;
         
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Phone= new JFXTreeTableColumn<>("Phone Number");
  
             Phone.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().farmer_phone;
         
                   }
               });             
            JFXTreeTableColumn<PaymentModel, String> Deliveries = new JFXTreeTableColumn<>("Total Deliveries");
  
             Deliveries.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().totaldeliveries;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Rate = new JFXTreeTableColumn<>("Pay Rate");
  
             Rate.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().rate;
                       
                   }
               });


            JFXTreeTableColumn<PaymentModel, String> Pay= new JFXTreeTableColumn<>("Total Pay");
  
             Pay.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().totalpay;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Credits= new JFXTreeTableColumn<>("Total Deductions");
  
             Credits.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().credits;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Net= new JFXTreeTableColumn<>("Net Pay");
  
             Net.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().netpay;
                       
                   }
               });
             
            JFXTreeTableColumn<PaymentModel, String> From= new JFXTreeTableColumn<>(" Pay From ");
  
             From.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().pay_from;
                       
                   }
               }); 
           
            JFXTreeTableColumn<PaymentModel, String> To = new JFXTreeTableColumn<>("Pay To");
  
            To.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().pay_from;
                       
                   }
               });
            JFXTreeTableColumn<PaymentModel, String> Status = new JFXTreeTableColumn<>("Status");
  
            Status.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<PaymentModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<PaymentModel, String> param) {
                       return param.getValue().getValue().status;
                       
                   }
               });
            
               PaymentsList = FXCollections.observableArrayList();
               
               TreeItem<PaymentModel> root = new RecursiveTreeItem<>(PaymentsList, RecursiveTreeObject::getChildren);
               paymentTableView.getColumns().addAll(payID, No, Name,Account, Phone, Deliveries, Rate,Pay,  Credits, Net, From, To, Status);
               paymentTableView.setRoot(root);
               paymentTableView.setShowRoot(false);
        
               addrowsToTable() ;
               
               
               searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                       paymentTableView.setPredicate(new Predicate<TreeItem<PaymentModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<PaymentModel> t) {
                               
                               boolean flag = t.getValue().pay_id.getValue().contains(newValue)
                                       || t.getValue().farmer_number.getValue().contains(newValue)
                                       || t.getValue().farmer_name.getValue().contains(newValue)
                                       || t.getValue().farmer_account.getValue().contains(newValue)
                                       || t.getValue().farmer_phone.getValue().contains(newValue)
                                       || t.getValue().totaldeliveries.getValue().contains(newValue)
                                       || t.getValue().rate.getValue().contains(newValue)
                                       || t.getValue().totalpay.getValue().contains(newValue)
                                       || t.getValue().credits.getValue().contains(newValue)   
                                       || t.getValue().netpay.getValue().contains(newValue) 
                                       || t.getValue().pay_from.getValue().contains(newValue)
                                       || t.getValue().pay_to.getValue().contains(newValue) 
                                       || t.getValue().status.getValue().contains(newValue) ;
                               return flag;
                               
                           }
                       });
                   }
                           
               });
    }

}
