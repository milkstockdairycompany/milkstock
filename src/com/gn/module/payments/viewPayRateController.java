package com.gn.module.payments;

import com.gn.global.util.Alerts;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.StageStyle;

public class viewPayRateController implements Initializable{
    
    ObservableList<PayRatesModel> RatesData= FXCollections.observableArrayList();
    
    Connection conn=com.gn.module.connection.Connect.Connect();
    PreparedStatement ps=null;
    ResultSet rs=null;
    
    @FXML
    private TableView<PayRatesModel> payRatesTable;

    @FXML
    private TableColumn<?, ?> ColumnId;

    @FXML
    private TableColumn<?, ?> ColumnFrom;

    @FXML
    private TableColumn<?, ?> ColumnTo;
    @FXML
    private TableColumn<?, ?> ColumnSh;
    
        @FXML
    private Label lb_error;

    @FXML
    private JFXDatePicker PayFromDP;

    @FXML
    private JFXDatePicker PayToDP;

    @FXML
    private JFXTextField RateTF;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
       LoadColumnData();
       LoadPayRatesData();
 
    }
    
    public void LoadColumnData(){
             ColumnId.setCellValueFactory(new PropertyValueFactory<>("payrateId"));
             ColumnFrom.setCellValueFactory(new PropertyValueFactory<>("payrateFrom"));
             ColumnTo.setCellValueFactory(new PropertyValueFactory<>("payrateTo"));
             ColumnSh.setCellValueFactory(new PropertyValueFactory<>("rateSh"));
        
        
    }
     public void LoadPayRatesData(){
        String query="select * from milk_payrates";
        try{
           
            ps= conn.prepareStatement(query);
            rs=ps.executeQuery();
            while(rs.next()){
    
                RatesData.add(new PayRatesModel(
                rs.getString("payrateId"),
                rs.getString("payrateFrom"),
                rs.getString("payrateTo"),
                rs.getString("rateSh")
                ));

                payRatesTable.setItems(RatesData);
                
            }
           
           
            ps.close();
            rs.close();
          
        }catch(Exception ex){
           Alerts.error("", ex.getMessage());
        
        }
     }
     @FXML
    private void Edit() throws IOException {
        Alerts.error("", "");
    }
    @FXML
    private void Delete () throws IOException {
        try{
         PayRatesModel select = (PayRatesModel)payRatesTable.getSelectionModel().getSelectedItem();
            
        }catch(Exception e){ 
            Alerts.info("Error", "You should select an item to delete");
        }
    }

  
    
}
