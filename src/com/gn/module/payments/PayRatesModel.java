/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gn.module.payments;

/**
 *
 * @author Alex
 */
public class PayRatesModel {
    private String payrateId;
    private String payrateFrom;
    private String payrateTo;
    private String rateSh;



    public PayRatesModel(String payrateId, String payrateFrom, String payrateTo, String rateSh) {
        this.payrateId = payrateId;
        this.payrateFrom = payrateFrom;
        this.payrateTo = payrateTo;
        this.rateSh = rateSh;
    }

    public String getPayrateId() {
        return payrateId;
    }

    public void setPayrateId(String payrateId) {
        this.payrateId = payrateId;
    }

    public String getPayrateFrom() {
        return payrateFrom;
    }

    
    public void setPayrateFrom(String payrateFrom) {
        this.payrateFrom = payrateFrom;
    }

    public String getPayrateTo() {
        return payrateTo;
    }

    public void setPayrateTo(String payrateTo) {
        this.payrateTo = payrateTo;
    }
    public String getRateSh() {
        return rateSh;
    }
    public void setRateSh(String rateSh) {
        this.rateSh = rateSh;
    }
}
