
package com.gn.module.milkrecords;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class DeliveryModel extends RecursiveTreeObject<DeliveryModel> {
    public   StringProperty delivery_id, farmer_number, delivery_date, delivery_amount, can_id, collector_name, vehicle,delivery_status  ;
    public DeliveryModel(String delivery_id, String farmer_number, String delivery_date, String delivery_amount, String can_id, String collector_name,String vehicle, String delivery_status ){
        this.delivery_id = new SimpleStringProperty(delivery_id);
        this.farmer_number = new SimpleStringProperty(farmer_number);
        this.delivery_date= new SimpleStringProperty(delivery_date);
        this.delivery_amount = new SimpleStringProperty(delivery_amount);
        this.can_id = new SimpleStringProperty(can_id);
        this.collector_name = new SimpleStringProperty(collector_name);
        this.vehicle = new SimpleStringProperty(vehicle);
        this.delivery_status= new SimpleStringProperty(delivery_status);
        
    }
public void setDelivery_id(String delivery_id){
    this.delivery_id= new SimpleStringProperty(delivery_id);
}
 public void setFarmer_number(String farmer_number) {
        this.farmer_number = new SimpleStringProperty(farmer_number);
    } 
  public void setDelivery_date(String delivery_date) {
        this.delivery_date = new SimpleStringProperty(delivery_date);
    }
   public void setCan_id(String can_id) {
        this.can_id = new SimpleStringProperty(can_id);
    }
   public void setCollector_name(String collector_name) {
        this.collector_name = new SimpleStringProperty(collector_name);
    }
   public void setVehice(String vehicle) {
        this.vehicle = new SimpleStringProperty(vehicle);
    }
    public void setDelivery_status(String delivery_status) {
        this.delivery_status = new SimpleStringProperty(delivery_status);
    }
    
    public String getDelivery_id() {
        return  delivery_id.get();
    }
     public String getFarmer_number() {
        return  farmer_number.get();
    }
      public String getDelivery_Date() {
        return  delivery_date.get();
    }
     public String getDelivery_amount() {
        return  delivery_amount.get();
    }
      public String getCan_id() {
        return  can_id.get();
    }
       public String getCollector_name() {
        return  collector_name.get();
    }
   public String getVehicle() {
        return  vehicle.get();
    }
      public String getDelivery_status() {
        return delivery_status.get();
    }
   
}
