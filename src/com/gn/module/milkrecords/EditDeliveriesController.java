
package com.gn.module.milkrecords;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class EditDeliveriesController implements Initializable{
    @FXML
    private Label lb_error, lb_number, lb_collector, lb_delivery, lb_amount, lb_can, lb_vehicle;

    @FXML
    private HBox box_farmerNumber, box_deliveryDate, box_amount, box_can, box_collector, box_vehicle;

    @FXML
    private JFXTextField  AmountTF;


    @FXML
    private JFXDatePicker DeliveryDateDP;

    @FXML
    private JFXComboBox<String> CollectorCB, VehicleCB, farmerNumberCB, CanCB;
    
 ObservableList<String> data= FXCollections.observableArrayList();
 ObservableList<String> vehicledata= FXCollections.observableArrayList();
 ObservableList<String> number= FXCollections.observableArrayList();
 ObservableList<String> can= FXCollections.observableArrayList();
 
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs=null;
 String id;
 
 
      public void transferData(String DeliveryID, String Number, String Date, String Amount, String Can, String Collector, String Vehicle, String Status) {
       id = DeliveryID;
       farmerNumberCB.setValue(Number);
      DeliveryDateDP.getEditor().setText(Date);
      AmountTF.setText(Amount);
      CanCB.setValue(Can);
      CollectorCB.setValue(Collector);
      VehicleCB.setValue(Vehicle);
      
       
    } 
      
                public void getCollectorName(){
                 try {
                     String sql="SELECT * from milk_stock.milk_staff WHERE staff_role = 'Milk Collector' ";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         data.add(rs.getString("staff_name"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
            public void getVehicle(){
                 try {
                     String sql="SELECT * from milk_stock.milk_vehicles";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         vehicledata.add(rs.getString("vehicle_regNo"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
           public void getCan(){
                 try {
                     String sql="SELECT * from milk_stock.milk_cans";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         can.add(rs.getString("milkcan_id"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
    //Get the farmer number and populates the combobox
         public void getFarmerNumber(){
                 try {
                     String sql="SELECT * from milk_stock.milk_farmers";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         number.add(rs.getString("farmer_number"));
                   } 
                      }catch(SQLException e){
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
      private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });
    

    }

      private void setupListeners(){
        farmerNumberCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validFarmerNumber()){
                if(!newValue){
                    Flash swing = new Flash(box_farmerNumber);
                    lb_number.setVisible(true);
                    new SlideInLeft(lb_number).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_farmerNumber.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_number.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
        
      DeliveryDateDP.focusedProperty().addListener((observable, oldValue, newValue) -> {            
          if(!validDate()){
                if(!newValue){
                    Flash swing = new Flash(box_deliveryDate);
                    lb_delivery.setVisible(true);
                    new SlideInLeft(lb_delivery).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_deliveryDate.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_delivery.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
           });
      AmountTF.focusedProperty().addListener((observable, oldValue, newValue) -> { 
            if(!validAmount()){
                if(!newValue){
                    Flash swing = new Flash(box_amount);
                    lb_amount.setVisible(true);
                    new SlideInLeft(lb_amount).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_amount.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_amount.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
            });
        CanCB.focusedProperty().addListener((observable, oldValue, newValue) -> { 
            if(!validCan()){
                if(!newValue){
                    Flash swing = new Flash(box_can);
                    lb_can.setVisible(true);
                    new SlideInLeft(lb_can).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_can.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_can.setVisible(false);
                }
            }else {
              lb_error.setVisible(true);
            }
            });
CollectorCB.focusedProperty().addListener((observable, oldValue, newValue) -> { 
            if(!validCollector()){
                if(!newValue){
                    Flash swing = new Flash(box_collector);
                    lb_collector.setVisible(true);
                    new SlideInLeft(lb_collector).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_collector.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_collector.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
          });
     
VehicleCB.focusedProperty().addListener((observable, oldValue, newValue) -> { 
            if(!validVehicle()){
                if(!newValue){
                    Flash swing = new Flash(box_vehicle);
                    lb_vehicle.setVisible(true);
                    new SlideInLeft(lb_vehicle).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_vehicle.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lb_vehicle.setVisible(false);
                }
            } else {
              lb_error.setVisible(true);
            }
          });   
 }
        private boolean validFarmerNumber(){
                   return  !farmerNumberCB.getSelectionModel().isEmpty();
            }
        private boolean validDate(){
                    return !DeliveryDateDP.getEditor().getText().isEmpty();
            }
        private boolean validAmount(){
                    return Mask.noInitSpace(AmountTF) &&Mask.noLetters( AmountTF) && Mask.noSymbols(AmountTF) && !AmountTF.getText().isEmpty() && AmountTF.getLength() > 1  ;
        }
       private boolean validCan(){
            return  !CanCB.getSelectionModel().isEmpty() ;         
        }
        private boolean validCollector(){
                    return  !CollectorCB.getSelectionModel().isEmpty();
        }
        private boolean validVehicle(){
                    return  !VehicleCB.getSelectionModel().isEmpty();
        }
        
        
    @Override
    public void initialize(URL location, ResourceBundle resources) {
      
        addEffect(farmerNumberCB);
        addEffect(CanCB);
        addEffect(AmountTF);
        addEffect(DeliveryDateDP);
        addEffect(CollectorCB);
        addEffect(VehicleCB);
        
        setupListeners();
        
            getCollectorName();
            getVehicle();
            getFarmerNumber();
            getCan();
            
            farmerNumberCB.setItems(number);
            CollectorCB.setItems(data);
            VehicleCB.setItems(vehicledata);
            CanCB.setItems(can);
    }
    @FXML
    private void updateDelivery(ActionEvent event){
        if(validFarmerNumber() && validDate() && validAmount() && validCan() && validCollector() && validVehicle() ){
     update();   
     //DeliveryList.add(new DeliveryModel(( farmerNumberCB.getValue(), DeliveryDateDP.getValue().toString() , AmountTF.getText(), CanCB.getValue(), CollectorCB.getValue())));

           
      Button btn = (Button) event.getSource();
      Stage stage = (Stage) btn.getScene().getWindow();
      stage.close();
        
        } else if (!validFarmerNumber()){
            lb_number.setVisible(true);
        } else if (!validDate()) {
            lb_delivery.setVisible(true);
        } else if(!validAmount()){
            lb_amount.setVisible(true);
        } else if(!validCan()){
            lb_can.setVisible(true);
        }else if(!validCollector()){
            lb_collector.setVisible(true);
        }else if(!validVehicle()){
            lb_vehicle.setVisible(true);
        }
        else{
            lb_error.setVisible(true);
        }
    }
    
    void update(){
          String sqlUpdate= "UPDATE milk_stock.milk_deliveries SET farmer_number= '"+farmerNumberCB.getValue()+"', delivery_date='"+DeliveryDateDP.getValue()+"', delivery_amount='"+AmountTF.getText()+"', can_id='"+ CanCB.getValue()+"', collector_name='"+CollectorCB.getValue()+"', vehicle_id='"+VehicleCB.getValue()+"' WHERE delivery_id = '"+id+"'";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlUpdate);

           
            stat.executeUpdate();
            
         
             Alerts.success("Success", farmerNumberCB.getValue() + " - delivery record updated succesfully"); 
     
 
           //FarmersController().refreshTable();
           
        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException | NumberFormatException | NullPointerException | Error ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
    }
 }
    
   @FXML
    private void Close(ActionEvent event){
       Button btn = (Button) event.getSource();
       Stage stage =(Stage) btn.getScene().getWindow();
       stage.close();
       
    }

}
