
package com.gn.module.milkrecords;

import com.gn.global.util.Alerts;
import com.gn.module.farmers.FarmersModel;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;


public class StaffViewDeliveriesController implements Initializable{
     @FXML
    private JFXTreeTableView<DeliveryModel> DeliveriesTableView;

    @FXML
    private JFXTextField searchTF;

    @FXML
    private JFXDrawer drawer;   
    
      ObservableList<DeliveryModel> DeliveryList;
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    
    

    @Override
    public void initialize(URL location, ResourceBundle resources) { 
       JFXTreeTableColumn<DeliveryModel, String> DeliveryID= new JFXTreeTableColumn<>("Delivery ID");
               
            DeliveryID.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().delivery_id;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Number= new JFXTreeTableColumn<>("Farmer Number");
               
             Number.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().farmer_number;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Date= new JFXTreeTableColumn<>("Delivery Date");
               
             Date.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().delivery_date;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Amount= new JFXTreeTableColumn<>("Delivery Amount");
               
             Amount.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().delivery_amount;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Can= new JFXTreeTableColumn<>("Milk Can");
               
            Can.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().can_id;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Collector= new JFXTreeTableColumn<>("Collector Name");
               
             Collector.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().collector_name;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Vehicle= new JFXTreeTableColumn<>("Vehicle");
               
             Vehicle.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().vehicle;
                       
                   }
               });
        JFXTreeTableColumn<DeliveryModel, String> Status= new JFXTreeTableColumn<>("Delivery Status");
               
             Status.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<DeliveryModel, String>, ObservableValue<String>>() {
                   
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<DeliveryModel, String> param) {
                       return param.getValue().getValue().delivery_status;
                       
                   }
               });
             
                DeliveryList = FXCollections.observableArrayList();
               
               TreeItem<DeliveryModel> root = new RecursiveTreeItem<DeliveryModel>(DeliveryList, RecursiveTreeObject::getChildren);
               DeliveriesTableView.getColumns().addAll(DeliveryID, Number, Date, Amount, Can, Collector,Vehicle ,Status);
               DeliveriesTableView.setRoot(root);
               DeliveriesTableView.setShowRoot(false);
               
               addrowsToTable();

                searchTF.textProperty().addListener(new ChangeListener<String>() {
                   
                   @Override
                   public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                       
                       DeliveriesTableView.setPredicate(new Predicate<TreeItem<DeliveryModel>>() {
                           
                           @Override
                           public boolean test(TreeItem<DeliveryModel> t) {
                               
                               boolean flag = t.getValue().delivery_id.getValue().contains(newValue)
                                       || t.getValue().farmer_number.getValue().contains(newValue)
                                       || t.getValue().delivery_date.getValue().contains(newValue)
                                       || t.getValue().delivery_amount.getValue().contains(newValue)
                                       || t.getValue().can_id.getValue().contains(newValue)
                                       || t.getValue().collector_name.getValue().contains(newValue)
                                       || t.getValue().vehicle.getValue().contains(newValue)
                                       || t.getValue().delivery_status.getValue().contains(newValue);
                                   
                               return flag;
                               
                           }
                       });
                   }
                });
          
              
    }
    
    
    public  void addrowsToTable() {

        String sqlSelect = "SELECT delivery_id, farmer_number, delivery_date, delivery_amount, can_id, collector_name, vehicle_id, delivery_status FROM milk_stock.milk_deliveries";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
              DeliveryList.add(new DeliveryModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5),result.getString(6), result.getString(7),result.getString(8)));

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }
      public  void refresh(){
          DeliveryList.clear();
          addrowsToTable();
      }
 

 
}
