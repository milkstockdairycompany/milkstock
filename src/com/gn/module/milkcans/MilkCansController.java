
package com.gn.module.milkcans;

import com.gn.global.util.Alerts;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.function.Predicate;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Callback;


public class MilkCansController implements Initializable{
    @FXML
    private JFXTextField  searchTF;

    @FXML
    private JFXTreeTableView<MilkCansModel> MilkCansTable;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static ResultSet result;
    
   static ObservableList<MilkCansModel> MilkCansList;
   

   @FXML
    void deleteCan(ActionEvent event) {
        try {
          
            String sqlSelect = " DELETE FROM milk_stock.milk_cans WHERE milkcan_id='" + MilkCansTable.getSelectionModel().getSelectedItem().getValue().getId() + "'";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);
            stat.executeUpdate();
            
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.info("Info", "Select the can details you want to delete");
        }catch(ArrayIndexOutOfBoundsException e){
            Alerts.error("Error", e.getMessage());
            
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
    }
    
    @FXML
    private void EditCan(ActionEvent event) throws IOException{
  try{
      TreeItem<MilkCansModel> can= (TreeItem<MilkCansModel>)MilkCansTable.getSelectionModel().getSelectedItem();
       FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/com/gn/module/milkcans/editmilkcan.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        
        Stage stage = new Stage();
        //BoxBlur = new BoxBlur(3,3,3);
        stage.setScene(new Scene(p));
        //Creates a modal over the primary stage without adding a view on the task bar
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
        
        
         //Get controller of card 
           EditMilkcanController card = loader.<EditMilkcanController>getController();
            //Pass  data  
           card.transferData(can.getValue().getId(),can.getValue().getMilkcan_id() ,can.getValue().getMilkcan_capacity(),can.getValue().getMilkcan_material());
        stage.show();
    }catch(NullPointerException b){
      
        Alerts.info("Info", "Select  the can details you want to update");
    }catch(IOException b){
        b.printStackTrace();
    }
    }
    @FXML
    private void AddCan(ActionEvent event){
     try{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/com/gn/module/milkcans/addmilkcan.fxml"));
        loader.load();
        Parent p = loader.getRoot();
        
        Stage stage = new Stage();
        //BoxBlur = new BoxBlur(3,3,3);
        stage.setScene(new Scene(p));
        //Creates a modal over the primary stage without adding a view on the task bar
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.initOwner(((Node)event.getSource()).getScene().getWindow());
       
        stage.show();
        
     }catch(IOException b){
         b.printStackTrace();
     }   
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
     JFXTreeTableColumn<MilkCansModel, String> Id = new JFXTreeTableColumn<>("Milk Can Id");

            Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkCansModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkCansModel, String> param) {
                return param.getValue().getValue().id;

            }
        });
       
    JFXTreeTableColumn<MilkCansModel, String> Name = new JFXTreeTableColumn<>("Milk Can Name");

            Name.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkCansModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkCansModel, String> param) {
                return param.getValue().getValue().milkcan_id;

            }
        });
    JFXTreeTableColumn<MilkCansModel, String> Capacity = new JFXTreeTableColumn<>("Milk Can Capacity");

            Capacity.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkCansModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkCansModel, String> param) {
                return param.getValue().getValue().milkcan_capacity;

            }
        });
            
    JFXTreeTableColumn<MilkCansModel, String> Material = new JFXTreeTableColumn<>("Milk Can Material");

            Material.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<MilkCansModel, String>, ObservableValue<String>>() {

            @Override
            public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<MilkCansModel, String> param) {
                return param.getValue().getValue().milkcan_material;

            }
        });        
               MilkCansList = FXCollections.observableArrayList(); 
        
        TreeItem<MilkCansModel> root = new RecursiveTreeItem<MilkCansModel>(MilkCansList, RecursiveTreeObject::getChildren);
        MilkCansTable.getColumns().addAll(Id, Name, Capacity, Material);
        MilkCansTable.setRoot(root);
        MilkCansTable.setShowRoot(false);  
        
         addrowsToTable();
         

        
                 searchTF.textProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
  
               MilkCansTable.setPredicate(new Predicate<TreeItem<MilkCansModel>>() {

                    @Override
                    public boolean test(TreeItem<MilkCansModel> t) {

                        boolean flag = t.getValue().id.getValue().contains(newValue)
                                || t.getValue().milkcan_id.getValue().contains(newValue)
                                || t.getValue().milkcan_capacity.getValue().contains(newValue)
                                || t.getValue().milkcan_material.getValue().contains(newValue);
                              
                        return flag;

                    }
                });
            }

        });  
    }
       static void addrowsToTable() {

        String sqlSelect = "SELECT id, milkcan_id, milkcan_capacity, milkcan_material FROM milk_stock.milk_cans";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
                MilkCansList.add(new MilkCansModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4)) );

            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    } 
     @FXML 
     public void refreshTable(){
         refresh();
     }
        
    public static void refresh(){
         MilkCansList.clear();
          addrowsToTable();
        
        }
    
    
      public void deleteCan(){
          
      } 
}
          
          

   


