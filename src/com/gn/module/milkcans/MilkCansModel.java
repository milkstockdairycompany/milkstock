
package com.gn.module.milkcans;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class MilkCansModel extends RecursiveTreeObject<MilkCansModel> {
    
    public StringProperty  id,milkcan_id, milkcan_capacity, milkcan_material;
    
     public MilkCansModel(String id, String milkcan_id, String milkcan_capacity, String milkcan_material){
         this.id=new SimpleStringProperty(id);
         this.milkcan_id = new SimpleStringProperty(milkcan_id);
          this.milkcan_capacity = new SimpleStringProperty(milkcan_capacity);
          this.milkcan_material = new SimpleStringProperty(milkcan_material);
         
     }
      public void setId(String id) {
        this.id = new SimpleStringProperty(id);
    }
    public void setMilkCan_id(String milkcan_id) {
        this.milkcan_id = new SimpleStringProperty(milkcan_id);
    }
    public void setMilkCan_capacity(String milkcan_capacity) {
        this.milkcan_capacity = new SimpleStringProperty (milkcan_capacity);
    }
    public void setMilkCan_material(String milkcan_material) {
        this.milkcan_material = new SimpleStringProperty (milkcan_material);
    }
    public String getId( ) {
        return id.get();
    }
    public String getMilkcan_id( ) {
        return milkcan_id.get();
    }

    public String getMilkcan_capacity() {
        return milkcan_capacity.get();
    }

    public String getMilkcan_material() {
       return milkcan_material.get();
    }

    
    
}
