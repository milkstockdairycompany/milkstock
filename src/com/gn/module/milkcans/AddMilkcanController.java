
package com.gn.module.milkcans;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.Duration;


public class AddMilkcanController implements Initializable{
    @FXML
    private JFXComboBox<String> milkcanmaterialCB;
    @FXML
    private Label milkcanIdLB, milkcanCapacityLB, milkcanMaterialLB, errorLB;
    @FXML
    private HBox boxID, boxMaterial, boxCapacity;
    @FXML
    private JFXTextField  milkcanIdTF,  milkcancapacityTF;
    
    @FXML
    private JFXButton AddCan;
    
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    
   // ActionEvent AddEvent = AddCan.getEvent();
     
    ObservableList<MilkCansModel> MilkCansList;
    private void addEffect(Node node){
        node.addEventFilter(MouseEvent.MOUSE_PRESSED, event -> {
            //rotateTransition.play();
            Pulse pulse = new Pulse(node.getParent());
            pulse.setDelay(Duration.millis(100));
            pulse.setSpeed(5);
            pulse.play();
            node.getParent().setStyle("-icon-color : -success; -fx-border-color : -success");
        });

        node.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!node.isFocused())
            node.getParent().setStyle("-icon-color : -dark-gray; -fx-border-color : transparent");
            else node.getParent().setStyle("-icon-color : -info; -fx-border-color : -info");
        });
    

    }
    private void setupListeners(){
        milkcanIdTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validMilkCanId()){
                if(!newValue){
                    Flash swing = new Flash(boxID);
                    milkcanIdLB.setText("Invalid Can Id ");
                    milkcanIdLB.setVisible(true);
                    new SlideInLeft(milkcanIdLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxID.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    milkcanIdLB.setVisible(false);
                }
            } else {
                errorLB.setVisible(false);
            }
        });
        milkcancapacityTF.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validMilkCanCapacity()){
                if(!newValue){
                    Flash swing = new Flash(boxCapacity);
                    milkcanCapacityLB.setText("Invalid Capacity");
                    milkcanCapacityLB.setVisible(true);
                    new SlideInLeft(milkcanCapacityLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxCapacity.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    milkcanCapacityLB.setVisible(false);
                }
            } else {
                errorLB.setVisible(false);
            }
        });
        milkcanmaterialCB.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validMilkCanMaterial()){
                if(!newValue){
                    Flash swing = new Flash(boxMaterial);
                    milkcanMaterialLB.setText("Invalid Material");
                    milkcanMaterialLB.setVisible(true);
                    new SlideInLeft(milkcanMaterialLB).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    boxMaterial.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    milkcanMaterialLB.setVisible(false);
                }
            } else {
               errorLB.setVisible(false);
            }
        });
    }
    private boolean validMilkCanId() {
     return  Mask.noSymbols(milkcanIdTF) && !milkcanIdTF.getText().isEmpty() && milkcanIdTF.getLength() > 2;
    }
    
     private boolean validMilkCanCapacity() {
     return Mask.noLetters(milkcancapacityTF) && !milkcancapacityTF.getText().isEmpty() && milkcancapacityTF.getLength() > 1   ; 
    }
     
    private boolean validMilkCanMaterial() {
     return  !milkcanmaterialCB.getSelectionModel().isEmpty(); 
    }
  @FXML
void addMilkCan(ActionEvent event){
    if(validMilkCanId() && validMilkCanCapacity() && validMilkCanMaterial()){
          Button btn = (Button) event.getSource();
        Stage stage = (Stage) btn.getScene().getWindow();
        stage.close();
        
     sqlInsert= "INSERT INTO milk_stock.milk_cans(milkcan_id, milkcan_capacity, milkcan_material) VALUES(?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, milkcanIdTF.getText());
            stat.setString(2, milkcancapacityTF.getText());
            stat.setString(3, milkcanmaterialCB.getValue());
            stat.executeUpdate();
            
       
     MilkCansController.refresh();
     
     Alerts.success("Success", milkcanIdTF.getText() + " - added succesfully"); 
            
   
        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        } 
      
    }else if (!validMilkCanId()){
       milkcanIdLB.setVisible(true);
    } else if (!validMilkCanCapacity()) {
        milkcanCapacityLB.setVisible(true);
    } else if(!validMilkCanMaterial()){
       milkcanMaterialLB.setVisible(true);
    }else{
       errorLB.setVisible(true); 
    }
}


 @FXML
      private void Close(ActionEvent event){
       Button btn = (Button) event.getSource();
      Stage stage =(Stage) btn.getScene().getWindow();
      stage.close();
       
    }
      
    @Override
    public void initialize(URL location, ResourceBundle resources) {
      addEffect(milkcanIdTF);
      addEffect(milkcancapacityTF);
      addEffect(milkcanmaterialCB);
      
      setupListeners();
      Mask.nameField(milkcanIdTF);
      
       milkcanmaterialCB.getItems().addAll("Plastic","Aluminium"); 
    }

  
    
}
