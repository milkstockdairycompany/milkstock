
package com.gn.module.credits;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Alex
 */
public class CreditsModel extends RecursiveTreeObject<CreditsModel>{
    
    public   StringProperty credit_id, farmer_number, credit_item, credit_amount, credit_status, credit_date ;
    
    public CreditsModel(String credit_id, String farmer_number, String credit_item, String credit_amount, String credit_status, String credit_date ){
          this.credit_id = new SimpleStringProperty(credit_id);
          this.farmer_number= new SimpleStringProperty(farmer_number);
          this.credit_item = new SimpleStringProperty(credit_item);
          this.credit_amount = new SimpleStringProperty(credit_amount);
          this.credit_status = new SimpleStringProperty(credit_status);
          this.credit_date = new SimpleStringProperty(credit_date);
    }

    public StringProperty getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(StringProperty credit_id) {
        this.credit_id = credit_id;
    }

    public StringProperty getFarmer_number() {
        return farmer_number;
    }

    public void setFarmer_number(StringProperty farmer_number) {
        this.farmer_number = farmer_number;
    }

    public StringProperty getCredit_item() {
        return credit_item;
    }

    public void setCredit_item(StringProperty credit_item) {
        this.credit_item = credit_item;
    }

    public StringProperty getCredit_amount() {
        return credit_amount;
    }

    public void setCredit_amount(StringProperty credit_amount) {
        this.credit_amount = credit_amount;
    }

    public StringProperty getCredit_status() {
        return credit_status;
    }

    public void setCredit_status(StringProperty credit_status) {
        this.credit_status = credit_status;
    }

    public StringProperty getCredit_date() {
        return credit_date;
    }

    public void setCredit_date(StringProperty credit_date) {
        this.credit_date = credit_date;
    }


    
    
}
