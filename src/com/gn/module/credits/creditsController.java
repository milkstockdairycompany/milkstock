package com.gn.module.credits;

import animatefx.animation.Flash;
import animatefx.animation.Pulse;
import animatefx.animation.SlideInLeft;
import static com.gn.global.util.AddEffect.addEffect;
import com.gn.global.util.Alerts;
import com.gn.global.util.Mask;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import javafx.util.Duration;

import com.itextpdf.text.Document;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import javafx.beans.property.StringProperty;

public class creditsController implements Initializable{

    @FXML
    private JFXTreeTableView< CreditsModel > creditsTableView;

    @FXML
    private JFXTextField searchTF,txtamount, txtitem;

    @FXML
    private JFXDrawer drawer;

    @FXML
    private Label lbl_error,lbl_amount, lbl_number ,lbl_item;

    @FXML
    private HBox box_number,box_item, box_amount;

    @FXML
    private JFXComboBox<String> NumberCB;

     ObservableList<String> data= FXCollections.observableArrayList();
     
    private static Connection conn = null;
    private static PreparedStatement stat = null;
    private static String url = "jdbc:mysql://localhost:3306";
    private static String Password = "";
    private static String username = "root";
    private static String sqlInsert;
    ResultSet result;
    ResultSet rs=null;
    
     Calendar timer= Calendar.getInstance();
     
    ObservableList<CreditsModel> CreditsList;
     
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
      addEffect(NumberCB);
      addEffect(txtitem);
      addEffect(txtamount);
      
     setupListeners();
     
     getFarmerNumber();
     NumberCB.setItems(data);
     
         JFXTreeTableColumn<CreditsModel, String> Id= new JFXTreeTableColumn<>("Credit Id");
             Id.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().credit_id;
                       
                   }
         });
     
         JFXTreeTableColumn<CreditsModel, String> Number= new JFXTreeTableColumn<>("Farmer Number");
             Number.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().farmer_number;
                       
                   }
         });
        JFXTreeTableColumn<CreditsModel, String> Item= new JFXTreeTableColumn<>("Credit Item");
             Item.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().credit_item;
                       
                   }
         });
        JFXTreeTableColumn<CreditsModel, String> Amount= new JFXTreeTableColumn<>("Credit Amount");
             Amount.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().credit_amount;
                       
                   }
         });
             
          JFXTreeTableColumn<CreditsModel, String> Status= new JFXTreeTableColumn<>("Credit Status");
             Status.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().credit_status;
                       
                   }
         }); 
        
        JFXTreeTableColumn<CreditsModel, String> Date = new JFXTreeTableColumn<>("Credit Date");
             Date.setCellValueFactory(new Callback<TreeTableColumn.CellDataFeatures<CreditsModel, String>, ObservableValue<String>>() {     
                   @Override
                   public ObservableValue<String> call(TreeTableColumn.CellDataFeatures<CreditsModel, String> param) {
                       return param.getValue().getValue().credit_date;
                       
                   }
         });
             
            CreditsList = FXCollections.observableArrayList();
               
               TreeItem<CreditsModel> root = new RecursiveTreeItem<>(CreditsList, RecursiveTreeObject::getChildren);
               creditsTableView.getColumns().addAll(Id, Number, Item, Amount, Status, Date );
               creditsTableView.setRoot(root);
               creditsTableView.setShowRoot(false);
              
               addrowsToTable();
               
    }

        private void setupListeners(){

       NumberCB .focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validNumber()){
                if(!newValue){
                    Flash swing = new Flash(box_number);
                    lbl_number.setVisible(true);
                    new SlideInLeft(lbl_number).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_number.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_number.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
       txtitem .focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validItem()){
                if(!newValue){
                    Flash swing = new Flash(box_item);
                    lbl_item.setVisible(true);
                    new SlideInLeft(lbl_item).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_item.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_item.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });
       txtamount .focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!validAmount()){
                if(!newValue){
                    Flash swing = new Flash(box_amount);
                    lbl_amount.setVisible(true);
                    new SlideInLeft(lbl_amount).play();
                    swing.setDelay(Duration.millis(100));
                    swing.play();
                    box_amount.setStyle("-icon-color : -danger; -fx-border-color : -danger");
                } else {
                    lbl_amount.setVisible(false);
                }
            } else {
              lbl_error.setVisible(true);
            }
        });       
        }
        //Get the farmer number and populates the combobox
         public void getFarmerNumber(){
                 try {
                     String sql="SELECT * from milk_stock.milk_farmers";
                      

                   
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sql);
            
             rs = stat.executeQuery();
                   while(rs.next()){
                         data.add(rs.getString("farmer_number"));
                   } 
                      }catch(SQLException e){
                          e.printStackTrace();
                          Alerts.info("Error", e.getMessage());
                     }
                      
           }
         //validation of text field
              private boolean validNumber(){
                  return !NumberCB.getItems().equals(null);
                }
              private boolean validItem(){
                  return Mask.nameField(txtitem) && Mask.noSpaces(txtitem) && Mask.noInitSpace(txtitem) && Mask.noSymbols(txtitem) && !txtitem.getText().isEmpty()  ;
                }
              private boolean validAmount(){
                  return Mask.noSymbols(txtamount) && Mask.noInitSpace(txtamount) && Mask.noLetters(txtamount) && !txtamount.getText().isEmpty() && txtamount.getLength() > 1;
              } 
              
 //Add method -- it checks that all variables are corect and passes them to the insert method
    @FXML
        private void addCredit(){

            if (validNumber() && validItem() &&validAmount()){
                
                insert(NumberCB.getValue(), txtitem.getText(), txtamount.getText());
                
                refreshTable();
                
            } else if (!validNumber()){
            lbl_number.setVisible(true);
            
            }else if(!validItem()){
                lbl_item.setVisible(true);
            }else if(validAmount()){
                lbl_amount.setVisible(true);
            }
        }
              
              
         //insert method
   private void insert(String farmer_number, String credit_item, String credit_amount){

            timer.getTime();
            SimpleDateFormat tTime =new SimpleDateFormat("HH.mm.ss");           
           


            tTime.format(timer.getTime());
            SimpleDateFormat tdate= new SimpleDateFormat("yyyy-MM-dd");
            tdate.format(timer.getTime());

        String credit_status = "approved";
        
        sqlInsert= "INSERT INTO milk_stock.milk_credits( farmer_number, credit_item, credit_amount, credit_status, credit_date) VALUES(?,?,?,?,?)";
            try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlInsert);

            stat.setString(1, farmer_number);
            stat.setString(2, credit_item);
            stat.setString(3, credit_amount);
            stat.setString(4, credit_status);
            stat.setString(5, tdate.format(timer.getTime())+" "+tTime.format(timer.getTime()));  
          
            stat.executeUpdate();
            
        
         Alerts.success("Success " , "- added succesfully"); 
                
            clearFields();
   
        } catch (SQLException ex) {
            Alerts.error("Error", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Alerts.error("Error", ex.getMessage());

        } catch (NumberFormatException c) {
           Alerts.error("Error", c.getMessage());

        } catch (NullPointerException cc) {
            Alerts.error("Error",cc.getMessage());

        } catch (Error e) {
            Alerts.error("Error", e.getMessage());
        } catch (Exception f) {
            Alerts.error("Error",f.getMessage());

        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException ex) {
                Alerts.error("Error", ex.getMessage());
            }
        }
}
  @FXML
   private void editCredit(){
       
   }
   @FXML
   private void printCredit() throws FileNotFoundException, DocumentException{
             // 1. Create document
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);

        // 2. Create PdfWriter
        PdfWriter.getInstance(document, new FileOutputStream("result.pdf"));

        // 3. Open document
        document.open();

        // 4. Add content
        document.add(new Paragraph("MilkStock Dairy Company"));

        // 5. Close document
        document.close();
   }
   @FXML
   private void revokeCredit(){
        try {
            
            int index = creditsTableView.getSelectionModel().getSelectedIndex();
              
//      updates the status of the credit to revoked        
            String sqlSelect = "UPDATE  milk_stock.milk_credits SET credit_status= 'revoked' where credit_id='" +creditsTableView.getSelectionModel().getSelectedCells().get(index).getTreeItem().getValue().getCredit_id()+ "' " ;
            //String sqlSelect = "UPDATE  milk_stock.milk_credits SET credit_status= 'revoked' where credit_id= 1 ";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);
            stat.executeUpdate();
            
           
            Alerts.success("Success", "Credit record revoked successfully ");
            refreshTable();
            
        } catch (SQLException r) {
            Alerts.error("Error" ,r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error", n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error", l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error", rr.getMessage());
            }
        }  
   }
     @FXML
   private void refreshTable(){
      CreditsList.clear();
      
      addrowsToTable();
      
   }
   
     @FXML
   private void deleteCredit() throws IndexOutOfBoundsException {
         try {
            int index = creditsTableView.getSelectionModel().getSelectedIndex();
            CreditsList.remove(index);
            String sqlSelect = "delete  from milk_stock.milk_credits where credit_id='" + creditsTableView.getSelectionModel().getSelectedCells().get(index).getTreeItem().getValue().getCredit_id() + "'";
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);
            stat.executeUpdate();
            
            Alerts.success("Success", "Credit record deleted successfully ");
           
        } catch (SQLException r) {
            Alerts.error("Error" ,r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error", n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error", l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error", rr.getMessage());
            }
        }
   }
   @FXML
   private void clearFields(){
       txtitem.clear();
       txtamount.clear();
   }
   
void addrowsToTable() {

        String sqlSelect = "SELECT  * FROM milk_stock.milk_credits";
 
        try {

            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, username, Password);
            stat = conn.prepareStatement(sqlSelect);

            result = stat.executeQuery();

            while (result.next()) {
               CreditsList.add(new CreditsModel(result.getString(1), result.getString(2), result.getString(3), result.getString(4), result.getString(5), result.getString(6) ));
            }
        } catch (SQLException r) {
            Alerts.error("Error", r.getMessage());
        } catch (ClassNotFoundException n) {
            Alerts.error("Error",n.getMessage());
        } catch (NullPointerException l) {
            Alerts.error("Error",l.getMessage());
        } finally {
            try {
                conn.close();
                stat.close();
            } catch (SQLException rr) {
                Alerts.error("Error",rr.getMessage());
            }
        }
       

    }
}
