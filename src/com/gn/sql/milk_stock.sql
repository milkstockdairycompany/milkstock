-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2019 at 08:29 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milk_stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `milk_farmers`
--

CREATE TABLE `milk_farmers` (
  `farmer_number` int(10) NOT NULL,
  `farmer_name` varchar(40) NOT NULL,
  `farmer_email` varchar(40) NOT NULL,
  `farmer_contact` int(15) NOT NULL,
  `farmer_id` varchar(20) NOT NULL,
  `farmer_account_number` int(20) NOT NULL,
  `station_id` int(3) NOT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_farmers`
--

INSERT INTO `milk_farmers` (`farmer_number`, `farmer_name`, `farmer_email`, `farmer_contact`, `farmer_id`, `farmer_account_number`, `station_id`, `added_date`) VALUES
(1, 'Alex', 'alex@gmail.com', 74434490, '34474423', 3100000, 1, '2019-07-14 12:58:19'),
(2, 'Dfnnnn', 'dfgvhjk@ghh.conm', 1000010, '12322', 1331, 1, '2019-07-14 15:28:09'),
(3, 'Deno', 'deno@gmail.com', 7545454, '1152125', 1451, 1, '2019-07-15 09:58:05');

-- --------------------------------------------------------

--
-- Table structure for table `milk_routes`
--

CREATE TABLE `milk_routes` (
  `route_id` int(10) NOT NULL,
  `route_name` varchar(40) NOT NULL,
  `route_start_point` varchar(40) NOT NULL,
  `route_end_point` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_routes`
--

INSERT INTO `milk_routes` (`route_id`, `route_name`, `route_start_point`, `route_end_point`) VALUES
(2, 'Kiahuria-Watuka', 'Watuka Shopping Center', 'Kiahuria Primary');

-- --------------------------------------------------------

--
-- Table structure for table `milk_stations`
--

CREATE TABLE `milk_stations` (
  `station_id` int(3) NOT NULL,
  `station_name` varchar(40) NOT NULL,
  `route_id` int(3) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_stations`
--

INSERT INTO `milk_stations` (`station_id`, `station_name`, `route_id`, `added_date`) VALUES
(1, 'ghhh', 3, '2019-06-27 18:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `milk_vehicles`
--

CREATE TABLE `milk_vehicles` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_regNo` varchar(10) NOT NULL,
  `vehicle_brand` varchar(10) NOT NULL,
  `vehicle_model` varchar(10) NOT NULL,
  `vehicle_color` text NOT NULL,
  `vehicle_type` text NOT NULL,
  `driver_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_vehicles`
--

INSERT INTO `milk_vehicles` (`vehicle_id`, `vehicle_regNo`, `vehicle_brand`, `vehicle_model`, `vehicle_color`, `vehicle_type`, `driver_id`) VALUES
(1, 'vbghn', 'Vbnm', 'Ghjhnj', 'Hjkhh', 'PickUp', 3),
(2, 'bnmvbnm', 'Bnmbn', 'Bnmbnm', 'Vbnm', 'PickUp', 2),
(3, 'KAW13L', 'Toyota', 'NZE 12', 'Blue', 'PickUp', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `milk_farmers`
--
ALTER TABLE `milk_farmers`
  ADD PRIMARY KEY (`farmer_number`),
  ADD UNIQUE KEY `farmer_contact` (`farmer_contact`),
  ADD UNIQUE KEY `farmer_id` (`farmer_id`),
  ADD UNIQUE KEY `farmer_account_number` (`farmer_account_number`),
  ADD UNIQUE KEY `farmer_email` (`farmer_email`);

--
-- Indexes for table `milk_routes`
--
ALTER TABLE `milk_routes`
  ADD PRIMARY KEY (`route_id`),
  ADD UNIQUE KEY `route_id` (`route_id`);

--
-- Indexes for table `milk_stations`
--
ALTER TABLE `milk_stations`
  ADD PRIMARY KEY (`station_id`);

--
-- Indexes for table `milk_vehicles`
--
ALTER TABLE `milk_vehicles`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_regNo` (`vehicle_regNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `milk_farmers`
--
ALTER TABLE `milk_farmers`
  MODIFY `farmer_number` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `milk_routes`
--
ALTER TABLE `milk_routes`
  MODIFY `route_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `milk_stations`
--
ALTER TABLE `milk_stations`
  MODIFY `station_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `milk_vehicles`
--
ALTER TABLE `milk_vehicles`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
