-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2019 at 07:23 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `milk_stock`
--

-- --------------------------------------------------------

--
-- Table structure for table `milk_cans`
--

CREATE TABLE `milk_cans` (
  `id` int(10) NOT NULL,
  `milkcan_id` varchar(10) NOT NULL,
  `milkcan_capacity` int(3) NOT NULL,
  `milkcan_material` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_cans`
--

INSERT INTO `milk_cans` (`id`, `milkcan_id`, `milkcan_capacity`, `milkcan_material`) VALUES
(2, 'FGHJK', 120, 'Aluminium'),
(3, 'KLLLL', 520, 'Aluminium'),
(4, 'GYHUJIO', 5666, 'Aluminium'),
(5, 'DFGHJ', 455, 'Plastic'),
(6, 'GHJK', 455, 'Aluminium'),
(7, 'FGHJ', 455, 'Plastic'),
(9, 'FGHJKKK', 4454, 'Plastic'),
(10, 'GYHUJIOvv', 5666, 'Aluminium'),
(11, 'Fnbhhhh', 455, 'Plastic'),
(12, 'GHJKnn', 455, 'Aluminium'),
(14, 'GHJKnnff', 455, 'Aluminium'),
(15, 'GYHUJIOvvn', 5666, 'Aluminium'),
(16, 'Fghnm', 20, 'Aluminium'),
(18, 'Fghjktyu', 52, 'Aluminium');

-- --------------------------------------------------------

--
-- Table structure for table `milk_drivers`
--

CREATE TABLE `milk_drivers` (
  `driver_number` int(10) NOT NULL,
  `driver_name` varchar(40) NOT NULL,
  `driver_email` varchar(40) NOT NULL,
  `driver_contact` int(15) NOT NULL,
  `driver_id` varchar(20) NOT NULL,
  `driver_account_number` int(20) NOT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_drivers`
--

INSERT INTO `milk_drivers` (`driver_number`, `driver_name`, `driver_email`, `driver_contact`, `driver_id`, `driver_account_number`, `added_date`) VALUES
(1, 'Alex', 'alex@gmail.com', 7121212, '323232', 1424, '2019-07-25 07:22:51'),
(3, 'Ian Kakk', 'kakk@gmail.com', 745123584, '45454', 1234, '2019-07-25 09:39:48'),
(4, 'Edu Maina', 'edu@gmail.com', 7454545, '3452525', 123456, '2019-07-25 09:53:48'),
(5, 'Charlie', 'charles@gmail.com', 745626266, '23562356', 4587987, '2019-07-25 15:05:46'),
(6, 'Tito', 'titi@gmail.com', 12222222, '45555', 45555555, '2019-08-02 06:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `milk_farmers`
--

CREATE TABLE `milk_farmers` (
  `farmer_number` int(10) NOT NULL,
  `farmer_name` varchar(40) NOT NULL,
  `farmer_email` varchar(40) NOT NULL,
  `farmer_contact` int(15) NOT NULL,
  `farmer_id` varchar(20) NOT NULL,
  `farmer_account_number` int(20) NOT NULL,
  `station_id` int(3) NOT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_farmers`
--

INSERT INTO `milk_farmers` (`farmer_number`, `farmer_name`, `farmer_email`, `farmer_contact`, `farmer_id`, `farmer_account_number`, `station_id`, `added_date`) VALUES
(1, 'Alex', 'alex@gmail.com', 74434490, '34474423', 3100000, 1, '2019-07-14 12:58:19'),
(2, 'Dfnnnn', 'dfgvhjk@ghh.conm', 1000010, '12322', 1331, 1, '2019-07-14 15:28:09'),
(3, 'Deno', 'deno@gmail.com', 7545454, '1152125', 1451, 1, '2019-07-15 09:58:05'),
(4, 'Charles Mbogo', 'charles@gmail.com', 745858585, '123654', 125478546, 1, '2019-08-08 18:19:21'),
(6, 'Charles Mbogo', 'charles1@gmail.com', 745454545, '325325325', 4566789, 2, '2019-08-21 16:24:01'),
(7, 'Jackson Kiama', 'jack@alex.com', 745123123, '1212322222', 47412222, 1, '2019-09-13 17:19:30');

-- --------------------------------------------------------

--
-- Table structure for table `milk_men`
--

CREATE TABLE `milk_men` (
  `milkman_number` int(10) NOT NULL,
  `milkman_name` varchar(40) NOT NULL,
  `milkman_email` varchar(40) NOT NULL,
  `milkman_contact` int(15) NOT NULL,
  `milkman_id` varchar(20) NOT NULL,
  `milkman_account_number` int(20) NOT NULL,
  `added_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_men`
--

INSERT INTO `milk_men` (`milkman_number`, `milkman_name`, `milkman_email`, `milkman_contact`, `milkman_id`, `milkman_account_number`, `added_date`) VALUES
(1, 'Alex', 'alex@gmail.com', 745789878, '123456', 124585, '2019-07-28 18:26:24'),
(2, 'Gathuri', 'gathuri@gmail.com', 710331558, '325254', 320320458, '2019-07-28 18:27:57'),
(3, 'Nderitu', 'fghjkl@gmail.com', 122222222, '7777777', 5555555, '2019-09-13 17:20:41');

-- --------------------------------------------------------

--
-- Table structure for table `milk_routes`
--

CREATE TABLE `milk_routes` (
  `route_id` int(10) NOT NULL,
  `route_name` varchar(40) NOT NULL,
  `route_start_point` varchar(40) NOT NULL,
  `route_end_point` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_routes`
--

INSERT INTO `milk_routes` (`route_id`, `route_name`, `route_start_point`, `route_end_point`) VALUES
(2, 'Kiria-Watuka', 'Watuka Shopping Center', 'Kiria Primary '),
(3, 'Watuka- Embaringo', 'Watuka Shopping Center', 'Embaringo Shopping Center');

-- --------------------------------------------------------

--
-- Table structure for table `milk_stations`
--

CREATE TABLE `milk_stations` (
  `station_id` int(3) NOT NULL,
  `station_name` varchar(40) NOT NULL,
  `route_id` int(3) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_stations`
--

INSERT INTO `milk_stations` (`station_id`, `station_name`, `route_id`, `added_date`) VALUES
(1, 'ghhh', 3, '2019-06-27 18:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `milk_vehicles`
--

CREATE TABLE `milk_vehicles` (
  `vehicle_id` int(11) NOT NULL,
  `vehicle_regNo` varchar(10) NOT NULL,
  `vehicle_brand` varchar(10) NOT NULL,
  `vehicle_model` varchar(10) NOT NULL,
  `vehicle_color` text NOT NULL,
  `vehicle_type` text NOT NULL,
  `driver_id` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `milk_vehicles`
--

INSERT INTO `milk_vehicles` (`vehicle_id`, `vehicle_regNo`, `vehicle_brand`, `vehicle_model`, `vehicle_color`, `vehicle_type`, `driver_id`) VALUES
(1, 'vbghn', 'Vbnm', 'Ghjhnj', 'Hjkhh', 'PickUp', 3),
(2, 'bnmvbnm', 'Bnmbn', 'Bnmbnm', 'Vbnm', 'PickUp', 2),
(3, 'KAW13L', 'Toyota', 'NZE 12', 'Blue', 'PickUp', 2),
(4, 'KBL 456C', 'Ford', 'Ranger', 'Blue', 'PickUp', 3),
(5, 'KBF 456 M', 'Isuzu', 'Fr 455', 'White', 'Lorry', 3),
(6, 'KAZ 123 K', 'Mazda', 'NZE 89', 'Blue', 'PickUp', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `milk_cans`
--
ALTER TABLE `milk_cans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `milkcan_id` (`milkcan_id`);

--
-- Indexes for table `milk_drivers`
--
ALTER TABLE `milk_drivers`
  ADD PRIMARY KEY (`driver_number`),
  ADD UNIQUE KEY `driver_contact` (`driver_contact`),
  ADD UNIQUE KEY `driver_id` (`driver_id`),
  ADD UNIQUE KEY `driver_account_number` (`driver_account_number`),
  ADD UNIQUE KEY `driver_email` (`driver_email`);

--
-- Indexes for table `milk_farmers`
--
ALTER TABLE `milk_farmers`
  ADD PRIMARY KEY (`farmer_number`),
  ADD UNIQUE KEY `farmer_contact` (`farmer_contact`),
  ADD UNIQUE KEY `farmer_id` (`farmer_id`),
  ADD UNIQUE KEY `farmer_account_number` (`farmer_account_number`),
  ADD UNIQUE KEY `farmer_email` (`farmer_email`);

--
-- Indexes for table `milk_men`
--
ALTER TABLE `milk_men`
  ADD PRIMARY KEY (`milkman_number`),
  ADD UNIQUE KEY `milkman_contact` (`milkman_contact`),
  ADD UNIQUE KEY `milkman_id` (`milkman_id`),
  ADD UNIQUE KEY `milkman_account_number` (`milkman_account_number`),
  ADD UNIQUE KEY `milkman_email` (`milkman_email`);

--
-- Indexes for table `milk_routes`
--
ALTER TABLE `milk_routes`
  ADD PRIMARY KEY (`route_id`),
  ADD UNIQUE KEY `route_id` (`route_id`);

--
-- Indexes for table `milk_stations`
--
ALTER TABLE `milk_stations`
  ADD PRIMARY KEY (`station_id`);

--
-- Indexes for table `milk_vehicles`
--
ALTER TABLE `milk_vehicles`
  ADD PRIMARY KEY (`vehicle_id`),
  ADD UNIQUE KEY `vehicle_regNo` (`vehicle_regNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `milk_cans`
--
ALTER TABLE `milk_cans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `milk_drivers`
--
ALTER TABLE `milk_drivers`
  MODIFY `driver_number` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `milk_farmers`
--
ALTER TABLE `milk_farmers`
  MODIFY `farmer_number` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `milk_men`
--
ALTER TABLE `milk_men`
  MODIFY `milkman_number` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `milk_routes`
--
ALTER TABLE `milk_routes`
  MODIFY `route_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `milk_stations`
--
ALTER TABLE `milk_stations`
  MODIFY `station_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `milk_vehicles`
--
ALTER TABLE `milk_vehicles`
  MODIFY `vehicle_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
