
package com.gn.global.util;


  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    Connection conn = null;
    public static Connection conDB()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/milkstock", "root", "");
            System.out.println("Connection Successful");
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            System.err.println("Connect : "+ex.getMessage());
           return null;
        }
    }
}
  

