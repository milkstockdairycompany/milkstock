
package com.gn.global.plugin;

import com.gn.global.Section;
import com.gn.global.util.PasswordGenerator;
import com.gn.App;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;


public class SectionManager {
    private static Connection conn=null;
        private static PreparedStatement stat=null;
        private static String url="jdbc:mysql://localhost:3306";
        private static String Password="" ;
        private static String sername="root";
   
        public static ResultSet result;
     
     private static String log_id;
        
    private SectionManager(){
 
    }

    public static Section get(){
        try {
            File file = new File("dashboard.properties");
            Properties properties = new Properties();

            if(!file.exists()){
                file.createNewFile();
            }

            FileInputStream inputStream = new FileInputStream(file);
            properties.load(inputStream);

            return new Section(Boolean.valueOf(properties.getProperty("logged")), properties.getProperty("userLogged"));
        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }

    public static void save(String username ) {
        GenerateLogID(); 
        
          String sql ="INSERT INTO milk_stock.login_logs ( log_id, username) VALUES (?,?)";
           
        try {
            
           Class.forName("com.mysql.jdbc.Driver");
           conn=DriverManager.getConnection(url,sername,Password);
            stat = conn.prepareStatement(sql);

            stat.setString(1, log_id);
            stat.setString(2, username);
           
            stat.executeUpdate();
            
           
          System.out.println("Done");
           
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    public static void logout(){
        
      String sql =" UPDATE milk_stock.login_logs SET logout_time=DEFAULT WHERE log_id='"+log_id+"' " ;
        
        try {
            
           Class.forName("com.mysql.jdbc.Driver");
           conn=DriverManager.getConnection(url,sername,Password);
            stat = conn.prepareStatement(sql);

            stat.executeUpdate();
            
           
          System.out.println("Done");
           
        } catch (Exception e){
            e.printStackTrace();
        }   
    }
     private static void GenerateLogID(){
        PasswordGenerator pass = new PasswordGenerator.PasswordGeneratorBuilder()
       
        .useDigits(true)
        .build();
        
       log_id = pass.generate(8);
        
        System.out.println(log_id);
    
     }
}
