
package com.gn.global;


public class User {

    private String userName;
    private String fullName;
    private String userEmail;
    private String password;
    private String role;

    public User(){

    }

    public User(String userName, String fullName, String userEmail) {
        this.userName = userName;
        this.fullName = fullName;
        this.userEmail = userEmail;
    }
      public User(String userName, String fullName, String userEmail, String password) {
        this.userName = userName;
        this.fullName = fullName;
        this.userEmail = userEmail;
        this.password = password;
      
    }

    public User(String userName, String fullName, String userEmail, String role, String password) {
        this.userName = userName;
        this.fullName = fullName;
        this.userEmail = userEmail;
        this.password = password;
        this.role= role;
    }

  

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
           return "User[userName = " +userName+ ", fullName = " +fullName +", email = " +userEmail+ ",  role = " +role+ ", password = " + password +"]";
    }
}
